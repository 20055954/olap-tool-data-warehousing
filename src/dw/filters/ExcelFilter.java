package dw.filters;

//* By Andrey Shevyakov, 2015 * //

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class ExcelFilter extends FileFilter {

	// Check If Selected File Is An Excel Spreadsheet 
    public boolean accept(File f) {
    	
        if (f.isDirectory()) {
            return true;
        }

        String extension = Utils.getExtension(f);
        if (extension != null) {
            if (extension.equals(Utils.xlsx)) {
                    return true;
            } else {
                return false;
            }
        }

        return false;
        
    }

    // Dialog File Description
    public String getDescription() {
    	
        return "Excel Spreadsheets (.xlsx)";
        
    }
}