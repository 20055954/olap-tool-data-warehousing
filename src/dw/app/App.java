package dw.app;

//* By Andrey Shevyakov, 2015 * //

import java.util.ArrayList;
import dw.gui.GUI;
import dw.model.ManufactureSlip;
import dw.model.Product;
import dw.model.Promotion;
import dw.model.Sale;
import dw.model.Worker;

public class App {
	
	public static ArrayList <Worker> staff = new ArrayList <Worker>();
	public static ArrayList <String> salesLocations = new ArrayList <String>();
	public static ArrayList <Promotion> promotions = new ArrayList <Promotion>();
	public static ArrayList <Sale> sales = new ArrayList <Sale>();
	public static ArrayList <Product> products = new ArrayList <Product>();
	public static ArrayList <ManufactureSlip> productsManufactured = new ArrayList <ManufactureSlip>();
	
	public static GUI appGUI; 
	
	public static <V> void main (String [] args)
	{
		// Initializing GUI
		appGUI = new GUI();
	}
}
