package dw.gui;

//* By Andrey Shevyakov, 2015 * //

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;

import dw.app.App;
import dw.helper.ContextMenuMouseListener;
import dw.model.Product;
import dw.model.Sale;
import dw.model.Worker;

// Sales Info Retrieval (For One Location)
public class SalesMySQLPrompt extends JDialog {

	private static final long serialVersionUID = -7621434621614331089L;
	
	private int locationRow; 
	private java.sql.Connection connect;
	private Statement statement;
	private ResultSet resultSet;
	private boolean retrSuccess = false; 
	private ArrayList <Object[]> salesToInsert = new ArrayList<Object[]>();
	private int retrIndex = 0; 
	
	public SalesMySQLPrompt(String location, int locationRow) 
	{
		super(); 
		
		try { setIconImage(ImageIO.read(new File("res/icon.png")));}
		catch (IOException e) {}
		
		this.locationRow = locationRow;
		
		JPanel content = new JPanel (new GridLayout(6,1));
		
		JTextField [] fields = new JTextField[2];
		
		ContextMenuMouseListener rightClick = new ContextMenuMouseListener();
		
		for (int i = 0; i < fields.length; i++)
		{
			fields[i] = new JTextField();
			fields[i].setForeground(Color.BLACK);
			fields[i].addMouseListener(rightClick);
		}
		
		JPanel dbInfW = new JPanel (new GridLayout(1,2));
		JLabel dbInfDesc = new JLabel ("Database Location: ");
		JLabel dbInf = new JLabel(location);
		dbInfW.add(dbInfDesc);
		dbInfW.add(dbInf);
		JPanel connW = new JPanel (new GridLayout(1,2));
		JLabel connectionStatusDesc = new JLabel("Connection Status: ");
		JLabel connectionStatus = new JLabel("Not Attempted");
		connW.add(connectionStatusDesc);
		connW.add(connectionStatus);
		JPanel hostW = new JPanel (new GridLayout(1,2));
		JLabel hostCaption = new JLabel("Database: ");
		hostW.add(hostCaption);
		hostW.add(fields[0]);
		JPanel dbNameW = new JPanel (new GridLayout(1,2));
		JLabel dbNameCaption = new JLabel("Database Name: ");
		JTextField dbName = new JTextField();
		dbNameW.add(dbNameCaption);
		dbNameW.add(dbName);
		JPanel userW = new JPanel (new GridLayout(1,2));
		JLabel userCaption = new JLabel("Username: ");
		userW.add(userCaption);
		userW.add(fields[1]);
		JPanel passW = new JPanel (new GridLayout(1,2));
		JLabel passwordCaption = new JLabel("Password: ");
		JPasswordField password = new JPasswordField();
		password.setEchoChar('*');
		password.setForeground(Color.BLACK);
		passW.add(passwordCaption);
		passW.add(password);
		JButton connectB = new JButton("Connect");
		connectB.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		
		content.add(dbInfW);
		content.add(connW);
		content.add(hostW);
		content.add(userW);
		content.add(passW);
		content.add(connectB);
		
		// Help Hint
		fields[0].setToolTipText("Format: host/database_name");
		
		setTitle("MySQL Data Retrieval");
		setContentPane(content);
		pack();
		setVisible(true);
		setResizable(false);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
		
		connectB.addActionListener(new ActionListener(){

			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					Class.forName("com.mysql.jdbc.Driver").newInstance();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				for (int i = 0; i < fields.length; i++)
				{
					if (fields[i].getText().isEmpty() == true)
					{
						fields[i].setText("Required");
						fields[i].setForeground(Color.GRAY);
					}
				}
				
				if (password.getText().isEmpty() == true)
				{
					password.setEchoChar((char) 0);
					password.setText("Required");
					password.setForeground(Color.GRAY);
				}
				
				if (!fields[0].getText().equals("Required") && !fields[1].getText().equals("Required") && !password.getText().equals("Required"))
				{
				String serverConnD = "jdbc:mysql://" + fields[0].getText().trim();
				String userConnD = fields[1].getText().trim(); 
				String passwordConnD = password.getText().trim();
				
				try 
				{
					connect = DriverManager.getConnection(serverConnD, userConnD, passwordConnD);
					statement = (Statement) connect.createStatement();
					String query;
					
					query = "select * from sales";
					resultSet = (ResultSet) statement.executeQuery(query);
					
	    			try 
	    			{
	    				int toRemove = GUI.tempInsertsSales.get(locationRow);
	    				
	    				GUI.tempInsertsSales.set(locationRow, 0);
	    				
	    				while (toRemove > 0)
	    				{
	    					App.sales.remove(App.sales.size()-1);
	    					toRemove -= 1;
	    				}
	    			}
	    		    catch (Exception e){}
					
					while(resultSet.next())
	    			{
	    				int productID = resultSet.getInt("p_id");
	    				int sellerID = resultSet.getInt("s_id");
	    				int customerID = resultSet.getInt("c_id");
	    				Date date = resultSet.getDate("s_date");
	    				
	    				Object [] saleIds = {productID, sellerID, customerID, date}; 
	    				salesToInsert.add(saleIds);
	    				
	    			}
					
					query = "select * from staff";
					resultSet = (ResultSet) statement.executeQuery(query);
					
	    			try 
	    			{
	    				int toRemove = GUI.tempInsertsWorkers.get(locationRow);
	    				
	    				GUI.tempInsertsWorkers.set(locationRow, 0);
	    				
	    				while (toRemove > 0)
	    				{
	    					App.staff.remove(App.staff.size()-1);
	    					toRemove -= 1;
	    				}
	    			}
	    		    catch (Exception e){}
					
					while(resultSet.next()) {
					      String name = resultSet.getString("s_name").trim();
					      String email = resultSet.getString("s_email").trim();
					      
					      retrIndex++; 
					      
		    			  Worker seller = new Worker(name, email, "Sales", location);
		    			  
		    			  for (int i = 0; i < salesToInsert.size(); i++)
		    			  {
		    				  try
		    				  {
		    				  if ((int)salesToInsert.get(i)[1] == retrIndex) 
		    					  {
		    					  Object [] saleData = salesToInsert.get(i);
		    					  saleData[1] = seller;
		    					  salesToInsert.set(i, saleData);
		    					  }
		    				  }
		    				  catch (Exception e) {}
		    			  }
		    			  
		    			  if (seller.isUnique()) App.staff.add(seller);
		    				  
		    			  try 
		    			  {
		    				  GUI.tempInsertsWorkers.set(locationRow, GUI.tempInsertsWorkers.get(locationRow)+1);
		    			  }
		    			  catch (Exception e)
		    			  {
		    				  GUI.tempInsertsWorkers.add(locationRow, 1);
		    			  }
					}
					
					
					query = "select * from products";
					resultSet = (ResultSet) statement.executeQuery(query);
					
					retrIndex = 0;
					
					while (resultSet.next()){
						String name = resultSet.getString("name").trim();
						double price = resultSet.getDouble("price");
						
						retrIndex++;
						
						Product product = new Product(name, price, -1);
						
		    			  for (int i = 0; i < salesToInsert.size(); i++)
		    			  {
		    				  try
		    				  {
		    				  if ((int)salesToInsert.get(i)[0] == retrIndex) 
		    					  {
		    					  Object [] saleData = salesToInsert.get(i);
		    					  saleData[0] = product;
		    					  salesToInsert.set(i, saleData);
		    					  }
		    				  }
		    				  catch (Exception e){}
		    			  }
						
						if (App.products.isEmpty() == true)
	    				{
	    					App.products.add(product);
	    				}
	    				
	    				else
	    				{
	    					boolean unique = true;
	    					
	    					for (int i = 0; i < App.products.size(); i++)
	    					{
	    						if (App.products.get(i).getName().trim().toUpperCase().equals(name.toUpperCase()))
	    						{
	    							unique = false; 
	    							if (App.products.get(i).getPrice() == -1) App.products.get(i).setPrice(product.getPrice());
	    							break;
	    						}
	    					}
	    					
	    					if (unique == true) 
	    					{
	    						App.products.add(product);
	    					}
	    				}
					}
						
					for (Object [] saleData : salesToInsert)
					{
						Sale sale = new Sale ((Product)saleData[0], 1, location, (Worker)saleData[1], (Date)saleData[3], 0);
						App.sales.add(sale);
						
		    			try 
		    			{
		    				GUI.tempInsertsSales.set(locationRow, GUI.tempInsertsSales.get(locationRow)+1);
		    			}
		    			catch (Exception e)
		    			{
		    				GUI.tempInsertsSales.add(locationRow, 1);
		    			}	
					}
	    			
					GUI.salesRetrSuccess[locationRow] = true;
					retrSuccess = true; 
					dispose();
					
				    boolean allDataRetrieved = true;
				    
				    for (boolean retrievalSuccess : GUI.salesRetrSuccess)
				    {
				    	if (retrievalSuccess == false) allDataRetrieved = false;
				    }
				    
				    if (allDataRetrieved == true)
				    {
				    	GUI.productionLoad(App.appGUI, dim);
				    }
				} 
				
				catch (SQLException e) 
				{
					e.printStackTrace();
					connectionStatus.setText("Failed");
					connectionStatus.setForeground(Color.RED);
					password.setText(null);
					GUI.salesRetrSuccess[locationRow] = false;
				}
				
			}
			}
		});
		
		// Key Listener
		ActionListener submitMySQL = new ActionListener(){

			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (fields[0].getText().isEmpty() == false &&
						fields[1].getText().isEmpty() == false &&
						password.getText().isEmpty() == false &&
						!fields[0].getText().equals("Required") &&
						!fields[1].getText().equals("Required") &&
						(!password.getText().equals("Required") && password.getForeground().equals(Color.BLACK)))
					connectB.doClick();
				{
					int focus = 2;

					for (int i = 0; i < fields.length; i++)
					{
						if (fields[i].getText().isEmpty() || fields[i].getText().equals("Required")) 
						{
								focus = i;
								break;
						}
					}
					
					if (focus != 2) fields[focus].requestFocus();
					else password.requestFocus();
				}
			}
			
		}; 
		
		// Adding Submit Listeners
		fields[0].addActionListener(submitMySQL);
		fields[1].addActionListener(submitMySQL);
		password.addActionListener(submitMySQL);
		
		
		// Removing Prompts
		for (int i = 0; i < fields.length; i++)
		{
			
		final int num = i; 
		fields[i].addFocusListener(new FocusListener() {

			@Override
			public void focusGained(FocusEvent arg0) {
				if (fields[num].getText().equals("Required"))
				{
					fields[num].setText(null);
					fields[num].setForeground(Color.BLACK);
				}
			}

			@Override
			public void focusLost(FocusEvent arg0) {}
			
		});
		}
		
		
		password.addFocusListener(new FocusListener () {

			@Override
			public void focusGained(FocusEvent arg0) {
				password.setEchoChar('*');
				password.setText(null);
				password.setForeground(Color.BLACK);
			}

			@Override
			public void focusLost(FocusEvent arg0) {}
			
		});
		
		// Resuming The Workflow On Closure
		addWindowListener(new WindowListener(){

			@Override
			public void windowActivated(WindowEvent arg0) {}

			@Override
			public void windowClosed(WindowEvent arg0) {onClosure();}

			@Override
			public void windowClosing(WindowEvent arg0) {onClosure();}

			@Override
			public void windowDeactivated(WindowEvent arg0) {}

			@Override
			public void windowDeiconified(WindowEvent arg0) {}

			@Override
			public void windowIconified(WindowEvent arg0) {}

			@Override
			public void windowOpened(WindowEvent arg0) {}
			
		});
	}
	
	private void onClosure()
	{
		App.appGUI.setEnabled(true);
		
		int elementChangePos = locationRow + 1; 
		
		if (retrSuccess == false)
		{
			if (elementChangePos == 1)
			{
				((JLabel)((JPanel)(App.appGUI.getContentPane().getComponent(3))).getComponent(1)).setText("Cancelled");
				((JPanel)(App.appGUI.getContentPane().getComponent(3))).getComponent(1).setForeground(Color.RED);;
			}
			else
			{
				((JLabel)((JPanel)(App.appGUI.getContentPane().getComponent((elementChangePos*3)+1))).getComponent(1)).setText("Cancelled");
				((JPanel)(App.appGUI.getContentPane().getComponent((elementChangePos*3)+1))).getComponent(1).setForeground(Color.RED);;
			}
		}
		
		else
		{
			if (elementChangePos == 1)
			{
				((JLabel)((JPanel)(App.appGUI.getContentPane().getComponent(3))).getComponent(1)).setText("Success");
				((JPanel)(App.appGUI.getContentPane().getComponent(3))).getComponent(1).setForeground(Color.GREEN);;
			}
			else
			{
				((JLabel)((JPanel)(App.appGUI.getContentPane().getComponent((elementChangePos*3)+1))).getComponent(1)).setText("Success");
				((JPanel)(App.appGUI.getContentPane().getComponent((elementChangePos*3)+1))).getComponent(1).setForeground(Color.GREEN);;
			}
		}
		
		App.appGUI.toFront();
	}
}
