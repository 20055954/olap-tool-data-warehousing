package dw.gui;

// * By Andrey Shevyakov, 2015 * //

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import dw.app.App;
import dw.db.RedisConnector;
import dw.filters.ExcelFilter;
import dw.helper.ContextMenuMouseListener;
import dw.listeners.ExcelActionListener;
import dw.model.Worker;
import dw.model.ManufactureSlip;
import dw.model.Product;
import dw.model.Promotion;
import dw.model.Sale;

@SuppressWarnings("deprecation")
public class GUI extends JFrame {

	private static final long serialVersionUID = 4169089824723506418L;

	private JPanel dataLoadPMarketing;
	private JPanel dataLoadPSales;
	private Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	public static ArrayList<Integer> tempInsertsWorkers = new ArrayList<Integer>(); 
	public static ArrayList<Integer> tempInsertsSales = new ArrayList<Integer>();
	public static boolean[] salesRetrSuccess;

	private static int selectedLoc;
	private static int selectedOption;
	private static int selectedUnit;
	private static int selectedLvl;
	private static int selectedMeasurer;

	public static JComboBox<String> units;

	public static String[] unitsSelected;

	public GUI() {
		// Marketing DB Loading (Redis)
		dataLoadPMarketing = new JPanel();
		dataLoadPMarketing.setLayout(new GridLayout(6, 1));

		JLabel header = new JLabel("Marketing Database Access", SwingConstants.CENTER);
		header.setBorder(BorderFactory.createBevelBorder(1));

		JButton connectB = new JButton("Connect");
		connectB.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		dataLoadPMarketing.add(header);

		JTextField[] fields = new JTextField[2];
		
		ContextMenuMouseListener rightClick = new ContextMenuMouseListener();
		
		for (int i = 0; i < fields.length; i++) {
			fields[i] = new JTextField();
			fields[i].setForeground(Color.BLACK);
			fields[i].addMouseListener(rightClick);
		}

		JPanel connW = new JPanel(new GridLayout(1, 2));
		JLabel connectionStatusDesc = new JLabel("Connection Status: ");
		JLabel connectionStatus = new JLabel("Not Attempted");
		connW.add(connectionStatusDesc);
		connW.add(connectionStatus);
		JPanel hostW = new JPanel(new GridLayout(1, 2));
		JLabel hostCaption = new JLabel("Host: ");
		hostW.add(hostCaption);
		hostW.add(fields[0]);
		JPanel portW = new JPanel(new GridLayout(1, 2));
		JLabel portCaption = new JLabel("Port: ");
		portW.add(portCaption);
		portW.add(fields[1]);
		JPanel passW = new JPanel(new GridLayout(1, 2));
		JLabel passwordCaption = new JLabel("Password: ");
		JPasswordField password = new JPasswordField();
		password.setEchoChar('*');
		password.setForeground(Color.BLACK);
		passW.add(passwordCaption);
		passW.add(password);

		// Key Listener
		ActionListener submitMarketing = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (fields[0].getText().isEmpty() == false && fields[1].getText().isEmpty() == false
						&& password.getText().isEmpty() == false && !fields[0].getText().equals("Required")
						&& !fields[1].getText().equals("Required")
						&& (!password.getText().equals("Required") && password.getForeground().equals(Color.BLACK)))
					connectB.doClick();
				else {
					int focus = 2;
					for (int i = 0; i < fields.length; i++) {
						if (fields[i].getText().isEmpty() || fields[i].getText().equals("Required")) {
							focus = i;
							break;
						}
					}

					if (focus != 2)
						fields[focus].requestFocus();
					else
						password.requestFocus();
				}
			}

		};

		fields[0].addActionListener(submitMarketing);
		fields[1].addActionListener(submitMarketing);
		password.addActionListener(submitMarketing);
		dataLoadPMarketing.add(connW);
		dataLoadPMarketing.add(hostW);
		dataLoadPMarketing.add(portW);
		dataLoadPMarketing.add(passW);

		dataLoadPMarketing.add(connectB);

		// Removing Prompts
		for (int i = 0; i < fields.length; i++) {
			final int num = i;
			fields[i].addFocusListener(new FocusListener() {

				@Override
				public void focusGained(FocusEvent arg0) {
					if (fields[num].getText().equals("Required")) {
						fields[num].setText(null);
						fields[num].setForeground(Color.BLACK);
					}
				}

				@Override
				public void focusLost(FocusEvent arg0) {
				}

			});
		}

		password.addFocusListener(new FocusListener() {

			@Override
			public void focusGained(FocusEvent arg0) {
				if (password.getText().equals("Required")) {
					password.setEchoChar('*');
					password.setText(null);
					password.setForeground(Color.BLACK);
				}
			}

			@Override
			public void focusLost(FocusEvent arg0) {
			}

		});

		// Connection Listener (Online)
		connectB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				for (int i = 0; i < fields.length; i++) {
					if (fields[i].getText().isEmpty()) {
						fields[i].setText("Required");
						fields[i].setForeground(Color.GRAY);
					}
					if (!fields[i].getText().matches("[-+]?\\d*\\.?\\d+") && i == 1) {
						fields[i].setText("Required");
						fields[i].setForeground(Color.GRAY);
					}
				}

				if (password.getText().isEmpty() == true) {
					password.setEchoChar((char) 0);
					password.setText("Required");
					password.setForeground(Color.GRAY);
				}

				if (!fields[0].getText().trim().equals("Required")
						&& fields[1].getText().matches("[-+]?\\d*\\.?\\d+") == true
						&& !password.getText().equals("Required")) {
					try {

						String hostNameIn = fields[0].getText().trim();
						int portIn = Integer.parseInt(fields[1].getText());
						String passwordIn = password.getText().trim();

						RedisConnector rc = new RedisConnector(hostNameIn, portIn, passwordIn);

						App.salesLocations.addAll(rc.retrieveSalesLocations());
						App.promotions.addAll(rc.retrievePromotions());
						rc.closeConnection();

						// Sales DBs Loading (MySQL, Excel)
						int rows = App.salesLocations.size(); // Number of Entries

						salesRetrSuccess = new boolean[rows];

						dataLoadPSales = new JPanel(new GridLayout(rows * 4, 6));

						JLabel[] salesLocsCaptions = new JLabel[rows];
						JButton[] mySqlConnectBs = new JButton[rows];
						JButton[] excelConnectBs = new JButton[rows];
						JLabel[] statusCaptions = new JLabel[rows];
						JLabel[] statuses = new JLabel[rows];

						JPanel[] bPanels = new JPanel[rows];
						JPanel[] statusPanels = new JPanel[rows];

						JLabel header = new JLabel("Sales Databases Access", SwingConstants.CENTER);
						header.setBorder(BorderFactory.createBevelBorder(1));

						for (int i = 0; i < rows; i++) {
							final int row = i;

							salesLocsCaptions[i] = new JLabel(App.salesLocations.get(i));
							mySqlConnectBs[i] = new JButton("Connect to MySQL Database");
							excelConnectBs[i] = new JButton("Use .xlsx File (Microsoft Excel)");
							statusCaptions[i] = new JLabel("Data Retrieval Status: ");
							statuses[i] = new JLabel("Unattempted");

							bPanels[i] = new JPanel(new GridLayout(1, 3));
							bPanels[i].add(excelConnectBs[i]);
							bPanels[i].add(mySqlConnectBs[i]);

							statusPanels[i] = new JPanel(new GridLayout(1, 2));
							statusPanels[i].add(statusCaptions[i]);
							statusPanels[i].add(statuses[i]);

							mySqlConnectBs[i].addActionListener(new ActionListener() {

								@Override
								public void actionPerformed(ActionEvent arg0) {
									SalesMySQLPrompt prompt = new SalesMySQLPrompt(App.salesLocations.get(row), row);
									prompt.show();
									App.appGUI.setEnabled(false);
								}

							});

						}

						dataLoadPSales.add(header);

						for (int i = 0; i < rows; i++) {
							dataLoadPSales.add(salesLocsCaptions[i]);
							dataLoadPSales.add(bPanels[i]);
							dataLoadPSales.add(statusPanels[i]);
							if (i != rows - 1)
								dataLoadPSales.add(new JPanel());
						}

						// Excel File Loading

						for (int i = 0; i < rows; i++) {
							final int locNum = i;

							excelConnectBs[i].addActionListener(new ExcelActionListener(App.salesLocations.get(i)) {
								@Override
								public void actionPerformed(ActionEvent arg0) {

									try {
										int toRemove = GUI.tempInsertsSales.get(locNum);

										GUI.tempInsertsSales.set(locNum, 0);

										while (toRemove > 0) {
											App.sales.remove(App.sales.size() - 1);
											toRemove -= 1;
										}
									} catch (Exception e) {
									}

									try {
										int toRemove = tempInsertsWorkers.get(locNum);

										tempInsertsWorkers.set(locNum, 0);

										while (toRemove > 0) {
											App.staff.remove(App.staff.size() - 1);
											toRemove -= 1;
										}
									} catch (Exception e) {
									}

									JFileChooser excelFC = new JFileChooser();
									excelFC.setFileFilter(new ExcelFilter());
									excelFC.setAcceptAllFileFilterUsed(false);
									int returnVal = excelFC.showDialog(GUI.this, "Load");
									if (returnVal == JFileChooser.APPROVE_OPTION) {
										File excelFile = excelFC.getSelectedFile();

										try {
											FileInputStream fs = new FileInputStream(excelFile);
											XSSFWorkbook wb = new XSSFWorkbook(fs);
											XSSFSheet sheet = wb.getSheetAt(0);

											ArrayList<String> attrs = new ArrayList<String>();

											XSSFRow row = sheet.getRow(0);

											Iterator<Cell> cells = row.cellIterator();
											while (cells.hasNext()) {
												XSSFCell cell = (XSSFCell) cells.next();
												attrs.add(cell.getStringCellValue() + ":" + cell.getColumnIndex());
											}

											for (String attr : attrs) {

												int index = Integer.parseInt(attr.split(":")[1]);

												for (Row r : sheet) {

													if (index == 5) {
														if (r.getCell(index) != null
																&& r.getCell(index).getRowIndex() != 0) {
															Cell name = r.getCell(index);

															String n = name.getStringCellValue().trim();

															Worker seller = new Worker(n, null, "Sales", location);

															if (seller.isUnique())
																App.staff.add(seller);

															try {
																tempInsertsWorkers.set(locNum,
																		tempInsertsWorkers.get(locNum) + 1);
															} catch (Exception e) {
																tempInsertsWorkers.add(locNum, 1);
															}
														}
													}

													if (index == 20) {
														if (r.getCell(index) != null
																&& r.getCell(index).getRowIndex() != 0) {
															Cell product = r.getCell(index);
															Cell qtSold = r.getCell(index + 1);
															Cell revS = r.getCell(index + 2);
															Cell sellerN = r.getCell(index + 4);
															Cell date = r.getCell(index + 5);

															String p = product.getStringCellValue().trim();
															int qt = (int) qtSold.getNumericCellValue();
															double rev = revS.getNumericCellValue();
															String s = sellerN.getStringCellValue().trim();
															Date d = date.getDateCellValue();

															Product pr = new Product(p, -1, -1);

															if (App.products.isEmpty() == true) {
																App.products.add(pr);
															}

															else {
																boolean unique = true;

																for (int i = 0; i < App.products.size(); i++) {
																	if (App.products.get(i).getName().trim()
																			.toUpperCase().equals(p.toUpperCase())) {
																		unique = false;
																		if (App.products.get(i).getPrice() == -1)
																			App.products.get(i).setPrice(pr.getPrice());
																		break;
																	}
																}

																if (unique == true) {
																	App.products.add(pr);

																}
															}

															Worker seller = null;

															for (int i = 0; i < App.staff.size(); i++) {
																Worker retr = App.staff.get(i);													

																if (retr.getLocation().equals(location) == true
																		&& retr.getName().equals(s)) {
																	seller = retr;
																	break;
																}
															}

															Sale sale = new Sale(pr, qt, location, seller, d, rev);

															if (App.sales.isEmpty() == true) {
																App.sales.add(sale);

																try {
																	GUI.tempInsertsSales.set(locNum,
																			GUI.tempInsertsSales.get(locNum) + 1);
																} catch (Exception e) {
																	GUI.tempInsertsSales.add(locNum, 1);
																}
															}

															else {
																boolean unique = true;

																for (int i = 0; i < App.sales.size(); i++) {
																	if (App.sales.get(i).same(sale)) {
																		unique = false;
																		break;
																	}
																}

																if (unique == true) {
																	App.sales.add(sale);
																	try {
																		GUI.tempInsertsSales.set(locNum,
																				GUI.tempInsertsSales.get(locNum) + 1);
																	} catch (Exception e) {
																		GUI.tempInsertsSales.add(locNum, 1);
																	}
																}
															}

														}
													}
												}
											}

											statuses[locNum].setText("Success");
											statuses[locNum].setForeground(Color.GREEN);
											salesRetrSuccess[locNum] = true;

											boolean allDataRetrieved = true;

											for (boolean retrievalSuccess : salesRetrSuccess) {
												if (retrievalSuccess == false)
													allDataRetrieved = false;
											}

											if (allDataRetrieved == true) {
												productionLoad(App.appGUI, dim);
											}

										}

										catch (Exception e) {
											e.printStackTrace();
											statuses[locNum].setText("Failed");
											statuses[locNum].setForeground(Color.RED);
											salesRetrSuccess[locNum] = false;
										}
									}
								}
							});
						}

						setContentPane(dataLoadPSales);
						pack();
						setSize((int) (getWidth() * 1.3), getHeight());
						setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);

					}

					catch (Exception e) {
						connectionStatus.setForeground(Color.RED);
						connectionStatus.setText("Failed");
						password.setText(null);
						e.printStackTrace();
					}
				}
			}

		});
		
		// Set Up Frame
		try { setIconImage(ImageIO.read(new File("res/icon.png")));}
		catch (IOException e) {}

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Data Warehouse");
		setResizable(false);
		setContentPane(dataLoadPMarketing);
		pack();
		setVisible(true);
		setSize((int) (getWidth() * 1.3), getHeight());
		setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);

	}

	public static void productionLoad(JFrame j, Dimension d) {
		j.toFront();

		JPanel productionLoadP = new JPanel(new GridLayout(8, 1));

		JTextField[] fields = new JTextField[4];

		ContextMenuMouseListener rightClick = new ContextMenuMouseListener();
		
		for (int i = 0; i < fields.length; i++) {
			fields[i] = new JTextField();
			fields[i].setForeground(Color.BLACK);
			fields[i].addMouseListener(rightClick);
		}

		JLabel header = new JLabel("Production Database Access", SwingConstants.CENTER);
		header.setBorder(BorderFactory.createBevelBorder(1));

		JPanel connW = new JPanel(new GridLayout(1, 2));
		JLabel connectionStatusDesc = new JLabel("Connection Status: ");
		JLabel connectionStatus = new JLabel("Not Attempted");
		connW.add(connectionStatusDesc);
		connW.add(connectionStatus);

		JPanel hostW = new JPanel(new GridLayout(1, 2));
		JLabel hostCaption = new JLabel("Host: ");
		hostW.add(hostCaption);
		hostW.add(fields[0]);

		JPanel portW = new JPanel(new GridLayout(1, 2));
		JLabel portCaption = new JLabel("Port: ");
		portW.add(portCaption);
		portW.add(fields[1]);

		JPanel dbW = new JPanel(new GridLayout(1, 2));
		JLabel dbCaption = new JLabel("Database: ");
		dbW.add(dbCaption);
		dbW.add(fields[2]);

		JPanel userW = new JPanel(new GridLayout(1, 2));
		JLabel userCaption = new JLabel("User: ");
		userW.add(userCaption);
		userW.add(fields[3]);

		JPanel passW = new JPanel(new GridLayout(1, 2));
		JLabel passwordCaption = new JLabel("Password: ");
		JPasswordField password = new JPasswordField();
		password.setEchoChar('*');
		password.setForeground(Color.BLACK);
		passW.add(passwordCaption);
		passW.add(password);

		JButton connectB = new JButton("Connect");

		productionLoadP.add(header);
		productionLoadP.add(connW);
		productionLoadP.add(hostW);
		productionLoadP.add(portW);
		productionLoadP.add(dbW);
		productionLoadP.add(userW);
		productionLoadP.add(passW);
		productionLoadP.add(connectB);

		j.setContentPane(productionLoadP);
		j.pack();
		j.setSize((int) (j.getWidth() * 1.5), j.getHeight());
		j.setLocation(d.width / 2 - j.getSize().width / 2, d.height / 2 - j.getSize().height / 2);

		// Key Listener
		ActionListener submitProduction = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				if (fields[0].getText().isEmpty() == false && fields[1].getText().isEmpty() == false
						&& fields[2].getText().isEmpty() == false && fields[3].getText().isEmpty() == false
						&& password.getText().isEmpty() == false && !fields[0].getText().equals("Required")
						&& !fields[1].getText().equals("Required") && !fields[2].getText().equals("Required")
						&& !fields[3].getText().equals("Required")
						&& (!password.getText().equals("Required") && password.getForeground().equals(Color.BLACK)))
					connectB.doClick();

				else {
					int focus = 4;

					for (int i = 0; i < fields.length; i++) {
						if (fields[i].getText().isEmpty() || fields[i].getText().equals("Required")) {
							focus = i;
							break;
						}
					}

					if (focus != 4)
						fields[focus].requestFocus();
					else
						password.requestFocus();
				}
			}
		};

		for (int i = 0; i < fields.length; i++) {
			fields[i].addActionListener(submitProduction);
		}

		password.addActionListener(submitProduction);

		// Prompt Removal
		for (int i = 0; i < fields.length; i++) {
			final int num = i;

			fields[i].addFocusListener(new FocusListener() {

				@Override
				public void focusGained(FocusEvent arg0) {
					if (fields[num].getText().equals("Required")) {
						fields[num].setText(null);
						fields[num].setForeground(Color.BLACK);
					}
				}

				@Override
				public void focusLost(FocusEvent arg0) {
				}

			});
		}

		password.addFocusListener(new FocusListener() {

			@Override
			public void focusGained(FocusEvent arg0) {
				if (password.getText().equals("Required")) {
					password.setEchoChar('*');
					password.setText(null);
					password.setForeground(Color.BLACK);
				}
			}

			@Override
			public void focusLost(FocusEvent arg0) {
			}

		});

		connectB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				for (int i = 0; i < fields.length; i++) {
					if (fields[i].getText().isEmpty() == true && i != 1) {
						fields[i].setText("Required");
						fields[i].setForeground(Color.GRAY);
					}

					else if (!fields[i].getText().matches("[-+]?\\d*\\.?\\d+") && i == 1) {
						fields[i].setText("Required");
						fields[i].setForeground(Color.GRAY);
					}

				}

				if (password.getText().isEmpty() == true) {
					password.setEchoChar((char) 0);
					password.setText("Required");
					password.setForeground(Color.GRAY);
				}

				if (!fields[0].getText().trim().equals("Required")
						&& fields[1].getText().matches("[-+]?\\d*\\.?\\d+") == true
						&& !fields[2].getText().trim().equals("Required")
						&& !fields[3].getText().trim().equals("Required")
						&& !password.getText().trim().equals("Required")) {
					try {
						String user = fields[3].getText().trim();
						String pass = password.getText().trim();
						String server = fields[0].getText().trim();
						int port = Integer.parseInt(fields[1].getText().trim());
						String dbName = fields[2].getText().trim();

						MongoClientURI cUri = new MongoClientURI(
								"mongodb://" + user + ":" + pass + "@" + server + ":" + port + "/" + dbName);
						MongoClient c = new MongoClient(cUri);

						DB db = c.getDB(dbName);

						DBCollection curColl;
						DBCursor cursor;

						curColl = db.getCollection("staff");
						cursor = curColl.find();

						int workersAdded = 0;

						try {
							while (cursor.hasNext()) {
								DBObject instance = cursor.next();
								String name = ((String) instance.get("name")).trim();
								String email = ((String) instance.get("email")).trim();
								String dept = "Storage & Manufacturing";
								String location = "Dublin, Co. Dublin";

								Worker worker = new Worker(name, email, dept,location);
								App.staff.add(worker);

								workersAdded++;
							}
						} catch (Exception e) {
						}

						curColl = db.getCollection("finished_products");
						cursor = curColl.find();

						try {
							while (cursor.hasNext()) {
								DBObject instance = cursor.next();
								String name = ((String) instance.get("name")).trim();
								double price = Double.parseDouble((String) instance.get("price"));
								int qtLeft = Integer.parseInt((String) instance.get("qt_left"));

								Product product = new Product(name, price, qtLeft);

								boolean unique = true;
								for (int i = 0; i < App.products.size(); i++) {
									Product toCompare = App.products.get(i);
									if ((product.getName().equals(toCompare.getName())) && toCompare.getPrice() == -1) {
										App.products.get(i).setPrice(product.getPrice());
										unique = false;
										break;
									}
								}

								if (unique == true)
									App.products.add(product);
							}
						} catch (Exception e) {
						}

						curColl = db.getCollection("products_manufactured");
						cursor = curColl.find();

						List<Worker> manufStaff = App.staff.subList(App.staff.size() - workersAdded, App.staff.size());

						try {
							while (cursor.hasNext()) {
								DBObject instance = cursor.next();
								String product = ((String) instance.get("product")).trim();
								String manufacturer = ((String) instance.get("manufacturer")).trim();
								String date = ((String) instance.get("date")).trim();

								Product pr = null;

								for (Product find : App.products) {
									if (find.getName().equals(product)) {
										pr = find;
										break;
									}

								}

								Worker wr = null;

								for (Worker find : manufStaff) {
									if (find.getName().equals(manufacturer)) {
										wr = find;
										break;
									}
								}

								SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
								Date manufDate = df.parse(date);

								ManufactureSlip slip = new ManufactureSlip(wr, pr, manufDate);
								App.productsManufactured.add(slip);
							}
						} catch (Exception e) {
						}

						manufStaff = null;

						analyticalScreen(j, d);

						c.close();
					} catch (Exception e) {
						e.printStackTrace();
						connectionStatus.setForeground(Color.RED);
						connectionStatus.setText("Failed");
						password.setText(null);
					}

				}
			}
		});
	}

	public static void analyticalScreen(JFrame j, Dimension d) {
		JLabel header = new JLabel("Data Analysis", SwingConstants.CENTER);
		header.setBorder(BorderFactory.createBevelBorder(1));

		JPanel analyticalP = new JPanel(new GridBagLayout());

		JPanel listPanel = new JPanel(new GridLayout(5, 1));
		JComboBox<String> locs = new JComboBox<String>();
		JComboBox<String> options = new JComboBox<String>();
		units = new JComboBox<String>();
		JComboBox<String> optionsLevel = new JComboBox<String>();
		JComboBox<String> perfMeasurers = new JComboBox<String>();
		listPanel.add(locs);
		listPanel.add(options);
		listPanel.add(units);
		listPanel.add(optionsLevel);
		listPanel.add(perfMeasurers);
		JTextField unitsComparedDisp = new JTextField();
		unitsComparedDisp.setEditable(false);
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		c.weighty = 0.1;
		c.weightx = 1;
		analyticalP.add(header, c);
		c.gridy = 1;
		analyticalP.add(listPanel, c);
		c.gridy = 3;
		c.weighty = 0.1;
		analyticalP.add(unitsComparedDisp, c);

		// Sale Locations
		locs.addItem("All Store Locations");
		for (String in : App.salesLocations)
			locs.addItem(in);

		// Sales Analysis
		options.addItem("Sales Performance");
		options.addItem("Manufacturing Performance");

		// Products To Display
		ArrayList <String> productNames = new ArrayList<String>();

		for (int i = 0; i < App.products.size(); i++) {
			productNames.add(App.products.get(i).getName());
		}

		Collections.sort(productNames);
		productNames.add(0, "All Products");
		productNames.add("Compare Products");
		for (String in : productNames)
			units.addItem(in);

		// Sales Levels
		String[] salesPerformanceLvls = { "By Year", "By Quarter (Current Year)", "By Month (Current Year)",
				"By Week (Current Quarter)", "By Day (Current Month)", "By Salesperson (Current Year)",
				"By Store (Current Year)", "By Promotion (Current Year)" };
		for (String in : salesPerformanceLvls)
			optionsLevel.addItem(in);

		// Performance Measurers
		String[] perfMeasurersValsSales = { "Products Sold", "Revenue Generated" };
		for (String in : perfMeasurersValsSales)
			perfMeasurers.addItem(in);

		// Options Listener
		selectedOption = options.getSelectedIndex();
		options.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (options.getSelectedIndex() != selectedOption) {
					perfMeasurers.removeAllItems();
					locs.removeAllItems();
					units.removeAllItems();
					optionsLevel.removeAllItems();

					for (String in : salesPerformanceLvls)
						optionsLevel.addItem(in);

					if (options.getSelectedItem().toString().equals("Sales Performance")) {
						for (String in : perfMeasurersValsSales)
							perfMeasurers.addItem(in);

						Collections.sort(productNames);
						productNames.add(0, "All Products");
						productNames.add("Compare Products");
						for (String in : productNames)
							units.addItem(in);
						
						locs.addItem("All Store Locations");
						for (String in : App.salesLocations)
							locs.addItem(in);

					}

					else {
						perfMeasurers.addItem("Products Manufactured");
						
						locs.setEnabled(true);

						ArrayList<String> manufUnits = new ArrayList<String>();

						for (ManufactureSlip ms : App.productsManufactured) {
							String pName = ms.getProductName();

							if (manufUnits.isEmpty())
								manufUnits.add(pName);

							else {
								boolean unique = true;

								for (String check : manufUnits) {
									if (check.equals(pName)) {
										unique = false;
										break;
									}
								}

								if (unique == true)
									manufUnits.add(pName);
							}
						}

						units.addItem("All Products");
						Collections.sort(manufUnits);
						for (String in : manufUnits)
							units.addItem(in);
						units.addItem("Compare Products");

						locs.addItem("Dublin");

						for (int i = 0; i < 3; i++)
							optionsLevel.removeItemAt(optionsLevel.getItemCount() - 1);

						optionsLevel.addItem("By Manufacturer (Current Year)");
					}

					createChartPanel(options.getSelectedItem().toString(), units.getSelectedItem().toString(),
							locs.getSelectedItem().toString(),
							optionsLevel.getSelectedItem().toString().split("\\(")[0].trim(),
							perfMeasurers.getSelectedItem().toString(), j, analyticalP, c, d, unitsSelected,
							unitsComparedDisp);
				}

				selectedOption = options.getSelectedIndex();
				selectedLoc = locs.getSelectedIndex();
				selectedUnit = units.getSelectedIndex();
				selectedLvl = optionsLevel.getSelectedIndex();
				selectedMeasurer = perfMeasurers.getSelectedIndex();
			}

		});

		// Location Listener
		selectedLoc = locs.getSelectedIndex();
		locs.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (locs.getSelectedIndex() != selectedLoc && options.getSelectedIndex() == selectedOption)
					createChartPanel(options.getSelectedItem().toString(), units.getSelectedItem().toString(),
							locs.getSelectedItem().toString(),
							optionsLevel.getSelectedItem().toString().split("\\(")[0].trim(),
							perfMeasurers.getSelectedItem().toString(), j, analyticalP, c, d, unitsSelected,
							unitsComparedDisp);
				selectedLoc = locs.getSelectedIndex();
			}

		});

		// Unit Listener
		selectedUnit = units.getSelectedIndex();
		units.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (options.getSelectedIndex() == selectedOption) {
					if (!units.getSelectedItem().toString().equals("Compare Products")) {
						if (units.getSelectedIndex() != selectedUnit)
							createChartPanel(options.getSelectedItem().toString(), units.getSelectedItem().toString(),
									locs.getSelectedItem().toString(),
									optionsLevel.getSelectedItem().toString().split("\\(")[0].trim(),
									perfMeasurers.getSelectedItem().toString(), j, analyticalP, c, d, unitsSelected,
									unitsComparedDisp);
					} else {
						if (options.getSelectedItem().toString().equals("Sales Performance"))
							new CompareUnitsPrompt(options.getSelectedItem().toString(),
									units.getSelectedItem().toString(), locs.getSelectedItem().toString(),
									optionsLevel.getSelectedItem().toString().split("\\(")[0].trim(),
									perfMeasurers.getSelectedItem().toString(), j, analyticalP, c, "Products",
									unitsComparedDisp, false);
						else
							new CompareUnitsPrompt(options.getSelectedItem().toString(),
									units.getSelectedItem().toString(), locs.getSelectedItem().toString(),
									optionsLevel.getSelectedItem().toString().split("\\(")[0].trim(),
									perfMeasurers.getSelectedItem().toString(), j, analyticalP, c, "Products",
									unitsComparedDisp, true);
					}
					selectedUnit = units.getSelectedIndex();
				}
			}

		});

		// Level Listener
		selectedLvl = optionsLevel.getSelectedIndex();
		optionsLevel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
					
				if (options.getSelectedIndex() == selectedOption) {
					if (optionsLevel.getSelectedIndex() == optionsLevel.getItemCount() - 2
							&& options.getSelectedItem().toString().equals("Sales Performance"))
						locs.setEnabled(false);
					else
						locs.setEnabled(true);
					
					if (optionsLevel.getSelectedIndex() != selectedLvl)
						createChartPanel(options.getSelectedItem().toString(), units.getSelectedItem().toString(),
								locs.getSelectedItem().toString(),
								optionsLevel.getSelectedItem().toString().split("\\(")[0].trim(),
								perfMeasurers.getSelectedItem().toString(), j, analyticalP, c, d, unitsSelected,
								unitsComparedDisp);
					
					selectedLvl = optionsLevel.getSelectedIndex();

					if (selectedLvl <= optionsLevel.getMaximumRowCount() - 4
							&& options.getSelectedItem().toString().equals("Sales Performance")
							&& units.getItemCount() == productNames.size() - 1)
						units.addItem("Compare Products");
					
					if (selectedLvl > optionsLevel.getMaximumRowCount() - 4 
							&& options.getSelectedItem().toString().equals("Sales Performance")
							&& units.getItemCount() == productNames.size())
					{
						units.setSelectedIndex(0);
						units.removeItemAt(productNames.size()-1);
					}
						
				}
			}

		});

		// Measurers Listener
		selectedMeasurer = perfMeasurers.getSelectedIndex();
		perfMeasurers.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (perfMeasurers.getSelectedIndex() != selectedMeasurer
						&& options.getSelectedIndex() == selectedOption)
					createChartPanel(options.getSelectedItem().toString(), units.getSelectedItem().toString(),
							locs.getSelectedItem().toString(),
							optionsLevel.getSelectedItem().toString().split("\\(")[0].trim(),
							perfMeasurers.getSelectedItem().toString(), j, analyticalP, c, d, unitsSelected,
							unitsComparedDisp);
				selectedMeasurer = perfMeasurers.getSelectedIndex();
			}

		});

		createChartPanel(options.getSelectedItem().toString(), units.getSelectedItem().toString(),
				locs.getSelectedItem().toString(), optionsLevel.getSelectedItem().toString().split("\\(")[0].trim(),
				perfMeasurers.getSelectedItem().toString(), j, analyticalP, c, d, unitsSelected, unitsComparedDisp);

		// Finalizing Windows Settings
		j.setContentPane(analyticalP);
		j.pack();
		j.setSize((int) j.getWidth(), j.getHeight());
		j.setLocation(d.width / 2 - j.getSize().width / 2, d.height / 2 - j.getSize().height / 2);
	}

	public static void createChartPanel(String option, String unit, String location, String lvl, String measurer,
			JFrame j, JPanel p, GridBagConstraints c, Dimension d, String[] manyUnits, JTextField unitsComparedDisp) {
		boolean proceed = true;

		if (p.getComponent(p.getComponentCount() - 1).getClass().toString().equals("class org.jfree.chart.ChartPanel"))
			p.remove(p.getComponentCount() - 1);
		try {
			if (((JLabel) p.getComponent(p.getComponentCount() - 1)).getText().equals("No Data"))
				p.remove(p.getComponentCount() - 1);
		} catch (Exception e) {
		}
		JFreeChart chart = null;

		// Sales Performance Charts
		if (option.equals("Sales Performance")) {
			
			// Products By Year
			if (lvl.equals("By Year")) {
				
				// Compare Selected Products By Year
				if (unit.equals("Compare Products")) {
					ArrayList<String[]> sales = new ArrayList<String[]>();

					for (String product : manyUnits) {
						for (Sale sale : App.sales) {

							if (location.equals("All Store Locations")) {
								if (sale.getProduct().getName().equals(product)) {
									String[] add = new String[4];
									add[0] = product;
									add[1] = String.valueOf(sale.getDate().getYear() + 1900);
									add[2] = String.valueOf(sale.getQtSold());
									if (measurer.equals("Revenue Generated"))
										add[3] = String.valueOf(sale.getRevenue());
									sales.add(add);
								}
							}

							else {
								if (sale.getLocation().equals(location)) {
									if (sale.getProduct().getName().equals(product)) {
										String[] add = new String[4];
										add[0] = product;
										add[1] = String.valueOf(sale.getDate().getYear() + 1900);
										add[2] = String.valueOf(sale.getQtSold());
										if (measurer.equals("Revenue Generated"))
											add[3] = String.valueOf(sale.getRevenue());
										sales.add(add);
									}
								}
							}
						}
					}

					ArrayList<String> yearProduct = new ArrayList<String>();

					for (int i = 0; i < sales.size(); i++) {
						if (!measurer.equals("Revenue Generated"))
							yearProduct.add(sales.get(i)[1] + "|" + sales.get(i)[0] + "|" + sales.get(i)[2]);
						else
							yearProduct.add(sales.get(i)[1] + "|" + sales.get(i)[0] + "|" + sales.get(i)[2] + "|"
									+ sales.get(i)[3]);
					}

					Set<String> uniqueYearProducts = new HashSet<String>(yearProduct);
					String[] uypProcessing = new String[uniqueYearProducts.size()];
					uypProcessing = uniqueYearProducts.toArray(uypProcessing);

					double[] salesByProductPerYear = new double[uniqueYearProducts.size()];

					for (int i = 0; i < uypProcessing.length; i++) {
						String compareName = uypProcessing[i].split("\\|")[1];
						String compareYear = uypProcessing[i].split("\\|")[0];

						for (int z = 0; z < yearProduct.size(); z++) {
							String salesInfo = yearProduct.get(z);
							String name = salesInfo.split("\\|")[1];
							String year = salesInfo.split("\\|")[0];
							int qtSold = Integer.parseInt(salesInfo.split("\\|")[2]);

							double rev = 0;

							if (measurer.equals("Revenue Generated"))
								rev = Double.parseDouble(salesInfo.split("\\|")[3]);

							if (year.equals(compareYear) && name.equals(compareName)) {
								if (!measurer.equals("Revenue Generated"))
									salesByProductPerYear[i] += qtSold;
								else
									salesByProductPerYear[i] += rev;
							}
						}
					}

					ArrayList<Integer> saleYears = new ArrayList<Integer>();

					for (int i = 0; i < uypProcessing.length; i++) {
						int year = Integer.parseInt(uypProcessing[i].split("\\|")[0]);
						if (saleYears.isEmpty())
							saleYears.add(year);

						boolean unique = true;

						for (int compareYear : saleYears) {
							if (compareYear == year) {
								unique = false;
								break;
							}
						}

						if (unique == true)
							saleYears.add(year);
					}

					// Check If Data Is There
					int dataIn = 0;

					if (!saleYears.isEmpty())
						dataIn = 1;

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int y : saleYears) {
							for (int i = 0; i < uypProcessing.length; i++) {
								String saleName = uypProcessing[i].split("\\|")[1];
								int saleYear = Integer.parseInt(uypProcessing[i].split("\\|")[0]);
								double saleNum = salesByProductPerYear[i];
								if (saleYear == y)
									dataset.addValue(saleNum, saleName, String.valueOf(y));
							}
						}

						String chartName;
						String measure;

						if (measurer.equals("Revenue Generated"))
							chartName = "Revenue By Year Per Product";
						else
							chartName = "Sales By Year Per Product";

						if (measurer.equals("Revenue Generated"))
							measure = measurer;
						else
							measure = "Number of Sales";

						chart = ChartFactory.createBarChart(chartName, "Products", measure, dataset,
								PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						if (measurer.equals("Revenue Generated")) {
							NumberAxis format = (NumberAxis) cp.getRangeAxis();
							format.setNumberFormatOverride(new DecimalFormat(("�#,##0.00")));
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"Product: {0}, Year: {1}, Revenue Generated: �{2}", NumberFormat.getInstance()));
						}

						else {
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"Product: {0}, Year: {1}, Quantity Sold: {2}", NumberFormat.getInstance()));
						}

					}

					else
						proceed = false;
				}

				// All Or One Products By Year
				else {
					ArrayList<String> sales = new ArrayList<String>();

					for (Sale sale : App.sales) {
						if (location.equals("All Store Locations")) {
							if (unit.equals("All Products") || sale.getProduct().getName().equals(unit)) {
								if (measurer.equals("Revenue Generated"))
									sales.add((sale.getDate().getYear() + 1900) + "|" + sale.getRevenue());
								else
									sales.add(sale.getDate().getYear() + 1900 + "|" + sale.getQtSold());
							}
						}

						else {
							if (sale.getLocation().equals(location)) {
								if (unit.equals("All Products") || sale.getProduct().getName().equals(unit)) {
									if (measurer.equals("Revenue Generated"))
										sales.add((sale.getDate().getYear() + 1900) + "|" + sale.getRevenue());
									else
										sales.add(sale.getDate().getYear() + 1900 + "|" + sale.getQtSold());
								}
							}
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					if (!sales.isEmpty())
						dataIn = 1;

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = 1900; i <= new Date().getYear() + 1900; i++) {
							for (String sale : sales) {
								if (Integer.parseInt(sale.split("\\|")[0]) == i) {

									final double num = Double.parseDouble(sale.split("\\|")[1]);
									String category = "Sales";

									if (!unit.equals("All Products"))
										category = category.concat(" (Product: " + unit + ")");

									try {
										dataset.incrementValue(num, String.valueOf(i), category);
									}

									catch (Exception e) {
										dataset.addValue(num, String.valueOf(i), category);
									}
								}
							}
						}

						String chartName;
						String measure;

						if (measurer.equals("Revenue Generated"))
							chartName = "Revenue By Year";
						else
							chartName = "Sales By Year";

						if (measurer.equals("Revenue Generated"))
							measure = measurer;
						else
							measure = "Number of Sales";

						chart = ChartFactory.createBarChart(chartName, "Year", measure, dataset,
								PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						if (measurer.equals("Revenue Generated")) {
							NumberAxis format = (NumberAxis) cp.getRangeAxis();
							format.setNumberFormatOverride(new DecimalFormat(("�#,##0.00")));
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"Year {0}, Revenue Generated: �{2}", NumberFormat.getInstance()));
						}

						else {
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"Year {0}, Products Sold: {2}", NumberFormat.getInstance()));
						}

					} else
						proceed = false;
				}
			}

			// Products By Quarter (Last Year)
			else if (lvl.equals("By Quarter")) {

				// Compare Selected Products By Quarter
				if (unit.equals("Compare Products")) {
					ArrayList<String> sales = new ArrayList<String>();

					int curY = new Date().getYear();

					for (String product : manyUnits) {
						for (Sale sale : App.sales) {

							if (location.equals("All Store Locations")) {
								if (sale.getProduct().getName().equals(product) && sale.getDate().getYear() == curY) {
									sales.add(product + "|" + sale.getDate().getMonth() + "|" + sale.getRevenue() + "|"
											+ sale.getQtSold());
								}
							}

							else {
								if (sale.getLocation().equals(location)) {
									if (sale.getProduct().getName().equals(product)
											&& sale.getDate().getYear() == curY) {
										sales.add(product + "|" + sale.getDate().getMonth() + "|" + sale.getRevenue()
												+ "|" + sale.getQtSold());
									}
								}
							}
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					if (sales.isEmpty() == false)
						dataIn = 1;

					if (dataIn != 0) {

						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = 0; i < 4; i++) {
							for (String sale : sales) {
								String saleName = sale.split("\\|")[0];
								double saleRev = Double.parseDouble(sale.split("\\|")[2]);
								int qtSold = Integer.parseInt(sale.split("\\|")[3]);
								int saleMonth = Integer.parseInt(sale.split("\\|")[1]);

								if (i == 0 && saleMonth < 3) {
									if (measurer.equals("Revenue Generated")) {
										try {
											dataset.incrementValue(saleRev, saleName, "1st Quarter");
										} catch (Exception e) {
											dataset.addValue(saleRev, saleName, "1st Quarter");
										}
									} else {
										try {
											dataset.incrementValue(qtSold, saleName, "1st Quarter");
										} catch (Exception e) {
											dataset.addValue(qtSold, saleName, "1st Quarter");
										}
									}
								} else if (i == 1 && saleMonth >= 3 && saleMonth < 6) {
									if (measurer.equals("Revenue Generated")) {
										try {
											dataset.incrementValue(saleRev, saleName, "2nd Quarter");
										} catch (Exception e) {
											dataset.addValue(saleRev, saleName, "2nd Quarter");
										}
									} else {
										try {
											dataset.incrementValue(qtSold, saleName, "2nd Quarter");
										} catch (Exception e) {
											dataset.addValue(qtSold, saleName, "2nd Quarter");
										}
									}
								} else if (i == 2 && saleMonth >= 6 && saleMonth < 9) {
									if (measurer.equals("Revenue Generated")) {
										try {
											dataset.incrementValue(saleRev, saleName, "3rd Quarter");
										} catch (Exception e) {
											dataset.addValue(saleRev, saleName, "3rd Quarter");
										}
									} else {
										try {
											dataset.incrementValue(qtSold, saleName, "3rd Quarter");
										} catch (Exception e) {
											dataset.addValue(qtSold, saleName, "3rd Quarter");
										}
									}
								} else if (i == 3 && saleMonth >= 9 && saleMonth < 12) {
									if (measurer.equals("Revenue Generated")) {
										try {
											dataset.incrementValue(saleRev, saleName, "4th Quarter");
										} catch (Exception e) {
											dataset.addValue(saleRev, saleName, "4th Quarter");
										}
									} else {
										try {
											dataset.incrementValue(qtSold, saleName, "4th Quarter");
										} catch (Exception e) {
											dataset.addValue(qtSold, saleName, "4th Quarter");
										}
									}
								}
							}
						}

						String chartName;
						String measure;

						if (measurer.equals("Revenue Generated"))
							chartName = "Revenue By Quarter";
						else
							chartName = "Sales By Quarter";

						if (measurer.equals("Revenue Generated"))
							measure = measurer;
						else
							measure = "Number of Sales";

						chart = ChartFactory.createBarChart(chartName, "Products", measure, dataset,
								PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						if (measurer.equals("Revenue Generated")) {
							NumberAxis format = (NumberAxis) cp.getRangeAxis();
							format.setNumberFormatOverride(new DecimalFormat(("�#,##0.00")));
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"Product: {0}, Quarter: {1}, Revenue Generated: �{2}", NumberFormat.getInstance()));
						}

						else {
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"Product: {0}, Quarter: {1}, Products Sold: {2}", NumberFormat.getInstance()));
						}

					} else
						proceed = false;

				}

				// All Or One Product By Quarter
				else {

					double[] salesPerQ = new double[4];

					for (Sale sale : App.sales) {
						if (sale.getDate().getYear() == new Date().getYear()) {
							if (location.equals("All Store Locations")) {
								if (unit.equals("All Products") || sale.getProduct().getName().equals(unit)) {
									int month = sale.getDate().getMonth();

									if (measurer.equals("Revenue Generated")) {
										if (month < 3)
											salesPerQ[0] += sale.getRevenue();
										else if (month < 6)
											salesPerQ[1] += sale.getRevenue();
										else if (month < 9)
											salesPerQ[2] += sale.getRevenue();
										else if (month < 12)
											salesPerQ[3] += sale.getRevenue();
									}

									else {
										if (month < 3)
											salesPerQ[0] += sale.getQtSold();
										else if (month < 6)
											salesPerQ[1] += sale.getQtSold();
										else if (month < 9)
											salesPerQ[2] += sale.getQtSold();
										else if (month < 12)
											salesPerQ[3] += sale.getQtSold();
									}

								}
							}

							else {
								if (sale.getLocation().equals(location)) {
									if (unit.equals("All Products") || sale.getProduct().getName().equals(unit)) {
										int month = sale.getDate().getMonth();

										if (measurer.equals("Revenue Generated")) {
											if (month < 3)
												salesPerQ[0] += sale.getRevenue();
											else if (month < 6)
												salesPerQ[1] += sale.getRevenue();
											else if (month < 9)
												salesPerQ[2] += sale.getRevenue();
											else if (month < 12)
												salesPerQ[3] += sale.getRevenue();
										}

										else {
											if (month < 3)
												salesPerQ[0] += sale.getQtSold();
											else if (month < 6)
												salesPerQ[1] += sale.getQtSold();
											else if (month < 9)
												salesPerQ[2] += sale.getQtSold();
											else if (month < 12)
												salesPerQ[3] += sale.getQtSold();
										}
									}
								}
							}
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					if (salesPerQ[0] + salesPerQ[1] + salesPerQ[2] + salesPerQ[3] > 0)
						dataIn = 1;

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();
						String category = "Sales";

						if (!unit.equals("All Products"))
							category = category.concat(" (Product: " + unit + ")");
						dataset.addValue(salesPerQ[0], "1st Quarter", category);
						dataset.addValue(salesPerQ[1], "2nd Quarter", category);
						dataset.addValue(salesPerQ[2], "3rd Quarter", category);
						dataset.addValue(salesPerQ[3], "4th Quarter", category);

						String chartName;
						String measure;

						if (measurer.equals("Revenue Generated"))
							chartName = "Revenue By Quarter";
						else
							chartName = "Sales By Quarter";

						if (measurer.equals("Revenue Generated"))
							measure = measurer;
						else
							measure = "Number of Sales";

						chart = ChartFactory.createBarChart(chartName, "Quarter", measure, dataset,
								PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						if (measurer.equals("Revenue Generated")) {
							NumberAxis format = (NumberAxis) cp.getRangeAxis();
							format.setNumberFormatOverride(new DecimalFormat(("�#,##0.00")));
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"{0}, Revenue Generated: �{2}", NumberFormat.getInstance()));
						}

						else {
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"{0}, Products Sold: {2}", NumberFormat.getInstance()));
						}
					} else
						proceed = false;
				}
			}

			// Products By Month (Last Year)
			else if (lvl.equals("By Month")) {
				// Compare Products (By Month)
				if (unit.equals("Compare Products")) {
					ArrayList<String> sales = new ArrayList<String>();

					int curY = new Date().getYear();

					for (String product : manyUnits) {
						for (Sale sale : App.sales) {

							if (location.equals("All Store Locations")) {
								if (sale.getProduct().getName().equals(product) && sale.getDate().getYear() == curY) {
									sales.add(product + "|" + sale.getDate().getMonth() + "|" + sale.getRevenue() + "|"
											+ sale.getQtSold());
								}
							}

							else {
								if (sale.getLocation().equals(location)) {
									if (sale.getProduct().getName().equals(product)
											&& sale.getDate().getYear() == curY) {
										sales.add(product + "|" + sale.getDate().getMonth() + "|" + sale.getRevenue()
												+ "|" + sale.getQtSold());
									}
								}
							}
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					if (sales.isEmpty() == false)
						dataIn = 1;

					if (dataIn != 0) {

						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = 0; i < 12; i++) {
							for (String sale : sales) {
								String saleName = sale.split("\\|")[0];
								int saleMonth = Integer.parseInt(sale.split("\\|")[1]);
								double saleRev = Double.parseDouble(sale.split("\\|")[2]);
								int saleQt = Integer.parseInt(sale.split("\\|")[3]);

								if (saleMonth == i) {
									if (measurer.equals("Revenue Generated")) {
										try {
											dataset.incrementValue(saleRev, saleName, intToMonth(saleMonth));
										}

										catch (Exception e) {
											dataset.addValue(saleRev, saleName, intToMonth(saleMonth));
										}
									}

									else {
										try {
											dataset.incrementValue(saleQt, saleName, intToMonth(saleMonth));
										}

										catch (Exception e) {
											dataset.addValue(saleQt, saleName, intToMonth(saleMonth));
										}
									}
								}
							}
						}

						String chartName;
						String measure;

						if (measurer.equals("Revenue Generated"))
							chartName = "Revenue By Month";
						else
							chartName = "Sales By Month";

						if (measurer.equals("Revenue Generated"))
							measure = measurer;
						else
							measure = "Number of Sales";

						chart = ChartFactory.createBarChart(chartName, "Products", measure, dataset,
								PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						if (measurer.equals("Revenue Generated")) {
							NumberAxis format = (NumberAxis) cp.getRangeAxis();
							format.setNumberFormatOverride(new DecimalFormat(("�#,##0.00")));
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"Product: {0}, Month: {1}, Revenue Generated: �{2}", NumberFormat.getInstance()));
						}

						else {
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"Product: {0}, Month: {1}, Products Sold: {2}", NumberFormat.getInstance()));
						}

					} else
						proceed = false;

				}

				// All Products or One Product (By Month)
				else {
					ArrayList<String> sales = new ArrayList<String>();

					for (Sale sale : App.sales) {
						if (sale.getDate().getYear() == new Date().getYear()) {
							if (location.equals("All Store Locations")) {
								if (unit.equals("All Products") || sale.getProduct().getName().equals(unit))
									sales.add(sale.getDate().getMonth() + "|" + sale.getRevenue() + "|"
											+ sale.getQtSold());
							} else {
								if (sale.getLocation().equals(location)) {
									if (unit.equals("All Products") || sale.getProduct().getName().equals(unit))
										sales.add(sale.getDate().getMonth() + "|" + sale.getRevenue() + "|"
												+ sale.getQtSold());
								}
							}
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					if (!sales.isEmpty())
						dataIn = 1;

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = 0; i < 12; i++) {
							for (String sale : sales) {
								int mCheck = Integer.parseInt(sale.split(("\\|"))[0]);

								if (mCheck == i) {
									final String month = intToMonth(Integer.parseInt(sale.split(("\\|"))[0]));
									final int salesNum = Integer.parseInt(sale.split("\\|")[2]);
									final double salesRev = Double.parseDouble(sale.split("\\|")[1]);

									String category = "Sales";
									if (!unit.equals("All Products"))
										category = category.concat(" (Product: " + unit + ")");
									if (measurer.equals("Revenue Generated")) {
										try {
											dataset.incrementValue(salesRev, month, category);
										}

										catch (Exception e) {
											dataset.addValue(salesRev, month, category);
										}
									}

									else {
										try {
											dataset.incrementValue(salesNum, month, category);
										}

										catch (Exception e) {
											dataset.addValue(salesNum, month, category);
										}
									}
								}
							}
						}

						String chartName;
						String measure;

						if (measurer.equals("Revenue Generated"))
							chartName = "Revenue By Month";
						else
							chartName = "Sales By Month";

						if (measurer.equals("Revenue Generated"))
							measure = measurer;
						else
							measure = "Number of Sales";

						chart = ChartFactory.createBarChart(chartName, "Month", measure, dataset,
								PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						if (measurer.equals("Revenue Generated")) {
							NumberAxis format = (NumberAxis) cp.getRangeAxis();
							format.setNumberFormatOverride(new DecimalFormat(("�#,##0.00")));
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"{0}, Revenue Generated: �{2}", NumberFormat.getInstance()));
						}

						else {
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"{0}, Products Sold: {2}", NumberFormat.getInstance()));
						}

					} else
						proceed = false;
				}
			}

			// Products By Week (Last Quarter)
			else if (lvl.equals("By Week")) {
				// Compare Products (By Week)
				if (unit.equals("Compare Products")) {
					ArrayList<String> sales = new ArrayList<String>();

					int curY = new Date().getYear();

					for (String product : manyUnits) {
						for (Sale sale : App.sales) {

							if (location.equals("All Store Locations")) {
								if (sale.getProduct().getName().equals(product) && sale.getDate().getYear() == curY) {
									Calendar calendar = Calendar.getInstance();
									calendar.setTime(sale.getDate());
									int woy = calendar.get(Calendar.WEEK_OF_YEAR);
									sales.add(sale.getProduct().getName() + "|" + woy + "|" + sale.getRevenue() + "|"
											+ sale.getQtSold());
								}
							}

							else {
								if (sale.getLocation().equals(location)) {
									if (sale.getProduct().getName().equals(product)
											&& sale.getDate().getYear() == curY) {
										Calendar calendar = Calendar.getInstance();
										calendar.setTime(sale.getDate());
										int woy = calendar.get(Calendar.WEEK_OF_YEAR);
										sales.add(sale.getProduct().getName() + "|" + woy + "|" + sale.getRevenue()
												+ "|" + sale.getQtSold());
									}
								}
							}
						}
					}

					int curM = new Date().getMonth();
					int curQ = 0;

					if (curM < 3)
						curQ = 1;
					else if (curM < 6)
						curQ = 2;
					else if (curM < 9)
						curQ = 3;
					else if (curM < 12)
						curQ = 4;

					int[] qWeeks = new int[2];

					Calendar cal = Calendar.getInstance(Locale.UK);
					cal.clear();
					cal.set(Calendar.YEAR, curY + 1900);

					if (curQ == 1) {
						cal.set(Calendar.MONTH, 2);
						cal.set(Calendar.DAY_OF_MONTH, 31);
						qWeeks[0] = 1;
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					else if (curQ == 2) {
						cal.set(Calendar.MONTH, 2);
						cal.set(Calendar.DAY_OF_MONTH, 31);
						qWeeks[0] = cal.get(Calendar.WEEK_OF_YEAR);
						cal.set(Calendar.MONTH, 5);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					else if (curQ == 3) {
						cal.set(Calendar.MONTH, 5);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[0] = cal.get(Calendar.WEEK_OF_YEAR);
						cal.set(Calendar.MONTH, 8);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					else if (curQ == 4) {
						cal.set(Calendar.MONTH, 8);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[0] = cal.get(Calendar.WEEK_OF_YEAR);
						cal.set(Calendar.MONTH, 11);
						cal.set(Calendar.DAY_OF_MONTH, 31);
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					// First Weeks Numbers
					cal.set(Calendar.MONTH, 3);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					int qTwoFirstWeek = cal.get(Calendar.WEEK_OF_YEAR);

					cal.set(Calendar.MONTH, 6);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					int qThreeFirstWeek = cal.get(Calendar.WEEK_OF_YEAR);

					cal.set(Calendar.MONTH, 9);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					int qFourFirstWeek = cal.get(Calendar.WEEK_OF_YEAR);

					// Check If Data Is There
					int dataIn = 0;

					for (String us : sales) {
						int sWeek = Integer.parseInt(us.split("\\|")[1]);
						if (sWeek >= qWeeks[0] && sWeek <= qWeeks[1]) {
							dataIn = 1;
							break;
						}
					}

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = qWeeks[0]; i <= qWeeks[1]; i++) {
							for (String sale : sales) {
								String saleName = sale.split("\\|")[0];
								int saleWeek = Integer.parseInt(sale.split("\\|")[1]);
								double rev = Double.parseDouble(sale.split("\\|")[2]);
								int qtSold = Integer.parseInt(sale.split("\\|")[3]);

								if (saleWeek == i) {
									int normalizeWeek = 0;

									switch (curQ) {
									case 2:
										normalizeWeek = qTwoFirstWeek;
										break;
									case 3:
										normalizeWeek = qThreeFirstWeek;
										break;
									case 4:
										normalizeWeek = qFourFirstWeek;
										break;
									}

									int dispWeek = saleWeek - normalizeWeek;
									if (dispWeek == 0)
										dispWeek++;

									if (dispWeek > 0) {
										if (measurer.equals("Revenue Generated")) {
											try {
												dataset.incrementValue(rev, saleName,
														"Week " + String.valueOf(dispWeek));
											}

											catch (Exception e) {
												dataset.addValue(rev, saleName, "Week " + String.valueOf(dispWeek));
											}
										}

										else {
											try {
												dataset.incrementValue(qtSold, saleName,
														"Week " + String.valueOf(dispWeek));
											}

											catch (Exception e) {
												dataset.addValue(qtSold, saleName, "Week " + String.valueOf(dispWeek));
											}
										}
									}
								}

							}
						}

						String chartName;
						String measure;

						if (measurer.equals("Revenue Generated"))
							chartName = "Revenue By Week";
						else
							chartName = "Sales By Week";

						if (measurer.equals("Revenue Generated"))
							measure = measurer;
						else
							measure = "Number of Sales";

						chart = ChartFactory.createBarChart(chartName, "Products", measure, dataset,
								PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						if (measurer.equals("Revenue Generated")) {
							NumberAxis format = (NumberAxis) cp.getRangeAxis();
							format.setNumberFormatOverride(new DecimalFormat(("�#,##0.00")));
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"Product: {0}, {1}, Revenue Generated: �{2}", NumberFormat.getInstance()));
						}

						else {
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"Product: {0}, {1}, Products Sold: {2}", NumberFormat.getInstance()));
						}

					}

					else
						proceed = false;
				}

				// All Products Or One Product (By Week)
				else {

					int curY = new Date().getYear();
					int curM = new Date().getMonth();
					int curQ = 0;

					if (curM < 3)
						curQ = 1;
					else if (curM < 6)
						curQ = 2;
					else if (curM < 9)
						curQ = 3;
					else if (curM < 12)
						curQ = 4;

					int[] qWeeks = new int[2];

					Calendar cal = Calendar.getInstance(Locale.UK);
					cal.clear();
					cal.set(Calendar.YEAR, curY + 1900);

					double[] dispVals = new double[cal.getActualMaximum(Calendar.WEEK_OF_YEAR) + 1];

					for (Sale sale : App.sales) {
						if (sale.getDate().getYear() == new Date().getYear()) {
							if (location.equals("All Store Locations")) {
								if (unit.equals("All Products") || sale.getProduct().getName().equals(unit)) {
									Calendar calendar = Calendar.getInstance();
									calendar.setTime(sale.getDate());
									int woy = calendar.get(Calendar.WEEK_OF_YEAR);
									if (measurer.equals("Revenue Generated"))
										dispVals[woy] += sale.getRevenue();
									else
										dispVals[woy] += sale.getQtSold();
								}
							} else {
								if (sale.getLocation().equals(location)) {
									if (unit.equals("All Products") || sale.getProduct().getName().equals(unit)) {
										Calendar calendar = Calendar.getInstance();
										calendar.setTime(sale.getDate());
										int woy = calendar.get(Calendar.WEEK_OF_YEAR);
										if (measurer.equals("Revenue Generated"))
											dispVals[woy] += sale.getRevenue();
										else
											dispVals[woy] += sale.getQtSold();
									}
								}
							}
						}

					}

					if (curQ == 1) {
						cal.set(Calendar.MONTH, 2);
						cal.set(Calendar.DAY_OF_MONTH, 31);
						qWeeks[0] = 1;
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					else if (curQ == 2) {
						cal.set(Calendar.MONTH, 2);
						cal.set(Calendar.DAY_OF_MONTH, 31);
						qWeeks[0] = cal.get(Calendar.WEEK_OF_YEAR);
						cal.set(Calendar.MONTH, 5);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					else if (curQ == 3) {
						cal.set(Calendar.MONTH, 5);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[0] = cal.get(Calendar.WEEK_OF_YEAR);
						cal.set(Calendar.MONTH, 8);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					else if (curQ == 4) {
						cal.set(Calendar.MONTH, 8);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[0] = cal.get(Calendar.WEEK_OF_YEAR);
						cal.set(Calendar.MONTH, 11);
						cal.set(Calendar.DAY_OF_MONTH, 31);
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					// First Weeks Numbers
					cal.set(Calendar.MONTH, 3);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					int qTwoFirstWeek = cal.get(Calendar.WEEK_OF_YEAR);

					cal.set(Calendar.MONTH, 6);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					int qThreeFirstWeek = cal.get(Calendar.WEEK_OF_YEAR);

					cal.set(Calendar.MONTH, 9);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					int qFourFirstWeek = cal.get(Calendar.WEEK_OF_YEAR);

					int dataIn = 0;

					for (int i = 0; i < dispVals.length; i++) {
						if (i >= qWeeks[0] && i <= qWeeks[1] && dispVals[i] > 0) {
							dataIn = 1;
							break;
						}
					}

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = 0; i < dispVals.length; i++) {
							double dispVal = dispVals[i];

							if (dispVal > 0) {
								int normalizeWeek = 0;

								switch (curQ) {
								case 2:
									normalizeWeek = qTwoFirstWeek;
									break;
								case 3:
									normalizeWeek = qThreeFirstWeek;
									break;
								case 4:
									normalizeWeek = qFourFirstWeek;
									break;
								}

								int dispWeek = i - normalizeWeek;
								if (dispWeek == 0)
									dispWeek++;

								String category = "Sales";
								if (!unit.equals("All Products"))
									category = category.concat(" (Product: " + unit + ")");

								if (dispWeek > 0) {
									if (measurer.equals("Revenue Generated")) {
										try {
											dataset.incrementValue(dispVal, "Week " + String.valueOf(dispWeek),
													category);
										}

										catch (Exception e) {
											dataset.addValue(dispVal, "Week " + String.valueOf(dispWeek), category);
										}
									}

									else {
										try {
											dataset.incrementValue(dispVal, "Week " + String.valueOf(dispWeek),
													category);
										}

										catch (Exception e) {
											dataset.addValue(dispVal, "Week " + String.valueOf(dispWeek), category);
										}
									}
								}
							}
						}

						String chartName;
						String measure;

						if (measurer.equals("Revenue Generated"))
							chartName = "Revenue By Week";
						else
							chartName = "Sales By Week";

						if (measurer.equals("Revenue Generated"))
							measure = measurer;
						else
							measure = "Number of Sales";

						chart = ChartFactory.createBarChart(chartName, "Week", measure, dataset,
								PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						if (measurer.equals("Revenue Generated")) {
							NumberAxis format = (NumberAxis) cp.getRangeAxis();
							format.setNumberFormatOverride(new DecimalFormat(("�#,##0.00")));
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"{0}, Revenue Generated: �{2}", NumberFormat.getInstance()));
						}

						else {
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"{0}, Products Sold: {2}", NumberFormat.getInstance()));
						}
					}

					else
						proceed = false;
				}
			}

			// Products By Day (Last Month)
			else if (lvl.equals("By Day")) {

				// Compare Products (By Day)
				if (unit.equals("Compare Products")) {
					ArrayList<String> sales = new ArrayList<String>();

					Calendar cal = Calendar.getInstance();
					cal.setTime(new Date());

					int curM = new Date().getMonth();
					int curY = new Date().getYear();
					int daysNum = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

					for (String product : manyUnits) {
						for (Sale sale : App.sales) {
							if (sale.getDate().getYear() == curY && sale.getDate().getMonth() == curM) {

								if (location.equals("All Store Locations")) {
									if (sale.getProduct().getName().equals(product)) {
										Calendar calendar = Calendar.getInstance();
										calendar.setTime(sale.getDate());
										int day = calendar.get(Calendar.DAY_OF_MONTH);
										sales.add(
												product + "|" + day + "|" + sale.getRevenue() + "|" + sale.getQtSold());
									}
								}

								else {
									if (sale.getLocation().equals(location)) {
										if (sale.getProduct().getName().equals(product)
												&& sale.getDate().getYear() == curY) {
											Calendar calendar = Calendar.getInstance();
											calendar.setTime(sale.getDate());
											int day = calendar.get(Calendar.DAY_OF_MONTH);
											sales.add(product + "|" + day + "|" + sale.getRevenue() + "|"
													+ sale.getQtSold());
										}
									}
								}
							}
						}
					}

					int dataIn = 0;

					if (!sales.isEmpty())
						dataIn = 1;

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						String month = intToMonth(new Date().getMonth());

						for (int i = 1; i <= daysNum; i++) {
							for (String sale : sales) {
								final String day = month + " " + i;
								String saleName = sale.split("\\|")[0];
								int saleDay = Integer.parseInt(sale.split("\\|")[1]);
								double saleRev = Double.parseDouble(sale.split("\\|")[2]);
								int qtSold = Integer.parseInt(sale.split("\\|")[3]);

								if (saleDay == i) {
									if (measurer.equals("Revenue Generated")) {
										try {
											dataset.incrementValue(saleRev, saleName, day);
										}

										catch (Exception e) {
											dataset.addValue(saleRev, saleName, day);
										}
									} else {
										try {
											dataset.incrementValue(qtSold, saleName, day);
										}

										catch (Exception e) {
											dataset.addValue(qtSold, saleName, day);
										}
									}
								}
							}
						}

						String chartName;
						String measure;

						if (measurer.equals("Revenue Generated"))
							chartName = "Revenue By Day";
						else
							chartName = "Sales By Day";

						if (measurer.equals("Revenue Generated"))
							measure = measurer;
						else
							measure = "Number of Sales";

						chart = ChartFactory.createBarChart(chartName, "Products", measure, dataset,
								PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						if (measurer.equals("Revenue Generated")) {
							NumberAxis format = (NumberAxis) cp.getRangeAxis();
							format.setNumberFormatOverride(new DecimalFormat(("�#,##0.00")));
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"Product: {0}, {1}, Revenue Generated: �{2}", NumberFormat.getInstance()));
						}

						else {
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"Product: {0}, {1}, Products Sold: {2}", NumberFormat.getInstance()));
						}

					}

					else
						proceed = false;
				}

				// All or One Product (By Day)
				else {

					Calendar cal = Calendar.getInstance();
					cal.setTime(new Date());

					int curM = new Date().getMonth();
					int curY = new Date().getYear();
					int[] salesByDay = new int[cal.getActualMaximum(Calendar.DAY_OF_MONTH)];
					double[] revByDay = new double[cal.getActualMaximum(Calendar.DAY_OF_MONTH)];

					for (Sale sale : App.sales) {
						if (sale.getDate().getYear() == curY) {
							if (location.equals("All Store Locations")) {
								if (unit.equals("All Products") || sale.getProduct().getName().equals(unit)) {
									cal.setTime(sale.getDate());
									if (sale.getDate().getMonth() == curM) {
										salesByDay[cal.get(Calendar.DAY_OF_MONTH) - 1] += sale.getQtSold();
										revByDay[cal.get(Calendar.DAY_OF_MONTH) - 1] += sale.getRevenue();
									}
								}
							}

							else {
								if (sale.getLocation().equals(location)) {
									if (unit.equals("All Products") || sale.getProduct().getName().equals(unit)) {
										cal.setTime(sale.getDate());
										if (sale.getDate().getMonth() == curM) {
											salesByDay[cal.get(Calendar.DAY_OF_MONTH) - 1] += sale.getQtSold();
											revByDay[cal.get(Calendar.DAY_OF_MONTH) - 1] += sale.getRevenue();
										}
									}
								}
							}
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					for (int check : salesByDay)
						dataIn += check;

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = 0; i < salesByDay.length; i++) {
							final String day = intToMonth(curM) + " " + (i + 1);
							final int salesNum = salesByDay[i];
							final double rev = revByDay[i];
							String category = "Sales";
							if (!unit.equals("All Products"))
								category = category.concat(" (Product: " + unit + ")");
							if (salesByDay[i] > 0 & revByDay[i] > 0) {
								if (measurer.equals("Revenue Generated"))
									dataset.addValue(rev, day, category);
								else
									dataset.addValue(salesNum, day, category);
							}
						}

						String chartName;
						String measure;

						if (measurer.equals("Revenue Generated"))
							chartName = "Revenue By Day";
						else
							chartName = "Sales By Day";

						if (measurer.equals("Revenue Generated"))
							measure = measurer;
						else
							measure = "Number of Sales";

						chart = ChartFactory.createBarChart(chartName, "Day", measure, dataset,
								PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						if (measurer.equals("Revenue Generated")) {
							NumberAxis format = (NumberAxis) cp.getRangeAxis();
							format.setNumberFormatOverride(new DecimalFormat(("�#,##0.00")));
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"{0}, Revenue Generated: �{2}", NumberFormat.getInstance()));
						}

						else {
							renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
									"{0}, Products Sold: {2}", NumberFormat.getInstance()));
						}
					}

					else
						proceed = false;
				}
			}
			
			// Products By Salesperson (Last Year)
			else if (lvl.equals("By Salesperson")) {
				
					ArrayList<Worker> salesPersons = new ArrayList<Worker>();
					ArrayList<Sale> allSales = new ArrayList<Sale>();

					for (Sale sale : App.sales) {
						if (location.equals("All Store Locations")) {
							if ((unit.equals("All Products") || sale.getProduct().getName().equals(unit))
									&& sale.getDate().getYear() == new Date().getYear()) {
								Worker worker = sale.getSeller();

								if (!salesPersons.contains(worker)) salesPersons.add(worker);

								allSales.add(sale);
							}
						}

						else {
							if (sale.getLocation().equals(location)) {
								if (unit.equals("All Products") || sale.getProduct().getName().equals(unit)) {
									Worker worker = sale.getSeller();

									if (!salesPersons.contains(worker)) salesPersons.add(worker);
									
									allSales.add(sale);
								}
							}
						}
					}

					String [] sellers = new String[salesPersons.size()];

					for (int i = 0; i < sellers.length; i++) {
						Worker s = salesPersons.get(i);
						sellers[i] = s.getName() + " (" + s.getLocation() + ")";
					}

					int[] salesByS = new int[sellers.length];
					double[] revByS = new double[sellers.length];

					for (Sale sale : allSales) {
						for (int i = 0; i < sellers.length; i++) {
							String seller = sellers[i];
							Worker s = sale.getSeller();
							if (seller.equals(s.getName() + " (" + s.getLocation() + ")")) {
								salesByS[i] += sale.getQtSold();
								revByS[i] += sale.getRevenue();
							}
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					for (int check : salesByS)
						dataIn += check;

					if (dataIn != 0) {
						DefaultPieDataset dataset = new DefaultPieDataset();

						
						for (int i = 0; i < sellers.length; i++) {
							final String worker = sellers[i];
							final int salesNum = salesByS[i];
							final double rev = revByS[i];
							if (measurer.equals("Revenue Generated"))
								dataset.setValue(worker.split("\\(")[0].trim(), rev);
							else
								dataset.setValue(worker.split("\\(")[0].trim(), salesNum);
						}

						String categoryP = " ";
						if (!unit.equals("All Products"))
							categoryP = categoryP.concat(" (Product: " + unit + ")");

						String chartName;

						if (measurer.equals("Revenue Generated"))
							chartName = "Revenue By Salesperson";
						else
							chartName = "Sales By Salesperson";

						chart = ChartFactory.createPieChart(chartName + categoryP, dataset, true, true, false);

						PiePlot pp = (PiePlot) chart.getPlot();

						if (measurer.equals("Revenue Generated")) {
							pp.setToolTipGenerator(
									new StandardPieToolTipGenerator("Salesperson: {0}, Revenue Generated: �{1} ({2})",
											new DecimalFormat(), new DecimalFormat("0%")));
							PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator("{2}",
									new DecimalFormat(), new DecimalFormat("0%"));
							pp.setLabelGenerator(gen);
						}

						else {
							pp.setToolTipGenerator(
									new StandardPieToolTipGenerator("Salesperson: {0}, Quantity Sold: {1} ({2})",
											new DecimalFormat(), new DecimalFormat("0%")));
							PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator("{2}",
									new DecimalFormat(), new DecimalFormat("0%"));
							pp.setLabelGenerator(gen);
						}

						pp.setSimpleLabels(true);
					}

					else
						proceed = false;
				
			}

			// Products By Store (Last Year)
			else if (lvl.equals("By Store")) {

					ArrayList<String> storeLocs = new ArrayList<String>();
					ArrayList<Sale> allSales = new ArrayList<Sale>();

					for (Sale sale : App.sales) {
						if (location.equals("All Store Locations")) {
							if ((unit.equals("All Products") || sale.getProduct().getName().equals(unit))
									&& sale.getDate().getYear() == new Date().getYear()) {
								String loc = sale.getLocation();

								boolean locExists = false;

								for (String s : storeLocs) {
									if (s.equals(loc)) {
										locExists = true;
										break;
									}
								}

								if (locExists == false)
									storeLocs.add(loc);

								allSales.add(sale);
							}
						}

						else {
							if (sale.getLocation().equals(location)) {
								if (unit.equals("All Products") || sale.getProduct().getName().equals(unit)) {
									String loc = sale.getLocation();

									boolean locExists = false;

									for (String s : storeLocs) {
										if (s.equals(loc)) {
											locExists = true;
											break;
										}
									}

									if (locExists == false)
										storeLocs.add(loc);

									allSales.add(sale);
								}
							}
						}
					}

					int[] salesByS = new int[storeLocs.size()];
					double[] revByS = new double[storeLocs.size()];

					for (Sale sale : allSales) {
						for (int i = 0; i < storeLocs.size(); i++) {
							String loc = storeLocs.get(i);
							String saleLoc = sale.getLocation();
							if (loc.equals(saleLoc)) {
								salesByS[i] += sale.getQtSold();
								revByS[i] += sale.getRevenue();
							}
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					for (int check : salesByS)
						dataIn += check;

					if (dataIn != 0) {
						DefaultPieDataset dataset = new DefaultPieDataset();

						for (int i = 0; i < storeLocs.size(); i++) {
							final String loc = storeLocs.get(i).trim();
							final int salesNum = salesByS[i];
							final double rev = revByS[i];
							if (measurer.equals("Revenue Generated"))
								dataset.setValue(loc, rev);
							else
								dataset.setValue(loc, salesNum);
						}

						String categoryP = " ";
						if (!unit.equals("All Products"))
							categoryP = categoryP.concat(" (Product: " + unit + ")");

						String chartName;

						if (measurer.equals("Revenue Generated"))
							chartName = "Revenue By Store";
						else
							chartName = "Sales By Store";

						chart = ChartFactory.createPieChart(chartName + categoryP, dataset, true, true, false);

						PiePlot pp = (PiePlot) chart.getPlot();
							
						if (measurer.equals("Revenue Generated")) {
							pp.setToolTipGenerator(
									new StandardPieToolTipGenerator("Store: {0}, Revenue Generated: �{1} ({2})",
											new DecimalFormat(), new DecimalFormat("0%")));
							PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator("{2}",
									new DecimalFormat(), new DecimalFormat("0%"));
							pp.setLabelGenerator(gen);
						}

						else {
							pp.setToolTipGenerator(
									new StandardPieToolTipGenerator("Store: {0}, Quantity Sold: {1} ({2})",
											new DecimalFormat(), new DecimalFormat("0%")));
							PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator("{2}",
									new DecimalFormat(), new DecimalFormat("0%"));
							pp.setLabelGenerator(gen);
						}

						pp.setSimpleLabels(true);
					}

					else
						proceed = false;
				
			}

			// Products By Promotion (Last Year)
			else if (lvl.equals("By Promotion")) {

					ArrayList<Double> saleAvg = new ArrayList<Double>();
					double[] promotionAvg = new double[App.promotions.size()];
					int[] promotionNum = new int[App.promotions.size()];
					int curY = new Date().getYear();

					for (Sale sale : App.sales) {
						if (sale.getDate().getYear() == curY) {
							if (location.equals("All Store Locations")) {
								if ((unit.equals("All Products") || sale.getProduct().getName().equals(unit))
										&& sale.getDate().getYear() == new Date().getYear()) {
									boolean isPromotion = false;

									for (int i = 0; i < App.promotions.size(); i++) {
										Promotion promotion = App.promotions.get(i);

										if (sale.getDate().compareTo(promotion.getDate()) == 0) {
											if (measurer.equals("Revenue Generated"))
												promotionAvg[i] += sale.getRevenue();
											else
												promotionAvg[i] += sale.getQtSold();

											promotionNum[i] += 1;

											isPromotion = true;
										}
									}

									if (isPromotion == false) {
										if (measurer.equals("Revenue Generated"))
											saleAvg.add(sale.getRevenue());
										else
											saleAvg.add(Double.parseDouble(String.valueOf(sale.getQtSold())));
									}
								}
							}

							else {
								if (sale.getLocation().equals(location)) {
									if (unit.equals("All Products") || sale.getProduct().getName().equals(unit)) {
										boolean isPromotion = false;

										for (int i = 0; i < App.promotions.size(); i++) {
											Promotion promotion = App.promotions.get(i);
											
											if (promotion.getLocs().contains(location.split(",")[1].trim())) {
												if (sale.getDate().compareTo(promotion.getDate()) == 0) {
													if (measurer.equals("Revenue Generated"))
														promotionAvg[i] += sale.getRevenue();
													else
														promotionAvg[i] += sale.getQtSold();

													promotionNum[i] += 1;

													isPromotion = true;
												}
											}
										}

										if (isPromotion == false) {
											if (measurer.equals("Revenue Generated"))
												saleAvg.add(sale.getRevenue());
											else
												saleAvg.add(Double.parseDouble(String.valueOf(sale.getQtSold())));
										}
									}
								}
							}
						}
					}

					double saleAvgVal = 0;

					for (Double in : saleAvg)
						saleAvgVal += in;

					saleAvgVal = saleAvgVal / saleAvg.size();

					double[] promotionAvgVal = new double[App.promotions.size()];

					for (int i = 0; i < promotionAvgVal.length; i++) {
						promotionAvgVal[i] = promotionAvg[i] / promotionNum[i];
					}

					// Check If Data Is There
					int dataIn = 0;

					for (double check : promotionAvgVal)
						dataIn += check;

					dataIn += saleAvgVal;

					if (dataIn != 0) {
						DefaultPieDataset dataset = new DefaultPieDataset();

						for (int i = 0; i <= promotionAvgVal.length; i++) {
							if (i != promotionAvgVal.length) {
								final String promotion = App.promotions.get(i).getDesc();
								final double val = promotionAvgVal[i];

								dataset.setValue(promotion, val);
							}

							else {
								final String promotion = "No Promotion";
								final double val = saleAvgVal;

								dataset.setValue(promotion, val);
							}
						}

						String categoryP = " ";
						if (!unit.equals("All Products"))
							categoryP = categoryP.concat(" (Product: " + unit + ")");

						String chartName;

						if (measurer.equals("Revenue Generated"))
							chartName = "Average Revenue By Promotion";
						else
							chartName = "Average Sale By Promotion";

						chart = ChartFactory.createPieChart(chartName + categoryP, dataset, true, true, false);

						PiePlot pp = (PiePlot) chart.getPlot();

						if (measurer.equals("Revenue Generated")) {
							pp.setToolTipGenerator(new StandardPieToolTipGenerator(
									"Promotion: {0}, Average Revenue Generated: �{1} ({2})", new DecimalFormat(),
									new DecimalFormat("0%")));
							PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator("{2}",
									new DecimalFormat(), new DecimalFormat("0%"));
							pp.setLabelGenerator(gen);
						}

						else {
							pp.setToolTipGenerator(
									new StandardPieToolTipGenerator("Promotion: {0}, Average Quantity Sold: {1} ({2})",
											new DecimalFormat(), new DecimalFormat("0%")));
							PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator("{2}",
									new DecimalFormat(), new DecimalFormat("0%"));
							pp.setLabelGenerator(gen);
						}

						pp.setSimpleLabels(true);
					}

					else
						proceed = false;
				}
			
		}

		// Manufacturing Performance Charts
		else if (option.equals("Manufacturing Performance")) {
			
			// Products Produced By Year
			if (lvl.equals("By Year")) {
				if (unit.equals("Compare Products")) {
					ArrayList<String> manufs = new ArrayList<String>();

					for (int i = 0; i < manyUnits.length; i++) {
						String product = manyUnits[i];

						for (ManufactureSlip ms : App.productsManufactured) {
							if (ms.getProduct().getName().equals(product)) {
								manufs.add(ms.getProduct().getName() + "|" + (ms.getDate().getYear() + 1900));
							}
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					if (!manufs.isEmpty())
						dataIn = 1;

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (String unitCheck : manyUnits) {
							for (String manufInfo : manufs) {
								String manufName = manufInfo.split("\\|")[0];

								if (unitCheck.equals(manufName)) {
									String year = manufInfo.split("\\|")[1];

									try {
										dataset.incrementValue(1, manufName, year);
									}

									catch (Exception e) {
										dataset.addValue(1, manufName, year);
									}
								}
							}
						}

						chart = ChartFactory.createBarChart("Products Manufactured By Year Per Product", "Products",
								"Quantity Manufactured", dataset, PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
								"Product: {0}, Year: {1}, Quantity Manufactured: {2}", NumberFormat.getInstance()));

					}

					else
						proceed = false;
				}

				else {
					int[] manufByY = new int[new Date().getYear() + 1];

					for (ManufactureSlip ms : App.productsManufactured) {
						if ((unit.equals("All Products") || ms.getProduct().getName().equals(unit))) {
							manufByY[ms.getDate().getYear()] += 1;
						}
					}

					int dataIn = 0;

					for (int check : manufByY) {
						if (check > 0) {
							dataIn = 1;
							break;
						}
					}

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = 0; i < new Date().getYear(); i++) {
							for (int z = 0; z < manufByY.length; z++) {
								if (z == i + 1) {

									final int manuf = manufByY[z];
									String category = "Products Manufactured";

									if (!unit.equals("All Products"))
										category = category.concat(" (Product: " + unit + ")");

									if (manuf != 0)
										dataset.addValue(manuf, String.valueOf(i + 1901), category);
								}
							}
						}

						chart = ChartFactory.createBarChart("Products Manufactured By Year", "Year",
								"Products Manufactured", dataset, PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
								"Year {0}, Products Manufactured: {2}", NumberFormat.getInstance()));

					}

					else
						proceed = false;

				}

			}

			// Products Produced By Quarter (Last Year)
			else if (lvl.equals("By Quarter")) {
				if (unit.equals("Compare Products")) {
					ArrayList<String> manufs = new ArrayList<String>();

					int curY = new Date().getYear();

					for (String product : manyUnits) {
						for (ManufactureSlip ms : App.productsManufactured) {
							if (ms.getProduct().getName().equals(product) && ms.getDate().getYear() == curY) {
								manufs.add(product + "|" + ms.getDate().getMonth());
							}
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					if (manufs.isEmpty() == false)
						dataIn = 1;

					if (dataIn != 0) {

						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = 0; i < 4; i++) {
							for (String ms : manufs) {
								String manufName = ms.split("\\|")[0];
								int manufMonth = Integer.parseInt(ms.split("\\|")[1]);

								if (i == 0 && manufMonth < 3) {
									try {
										dataset.incrementValue(1, manufName, "1st Quarter");
									} catch (Exception e) {
										dataset.addValue(1, manufName, "1st Quarter");
									}
								}

								else if (i == 1 && manufMonth >= 3 && manufMonth < 6) {
									try {
										dataset.incrementValue(1, manufName, "2nd Quarter");
									} catch (Exception e) {
										dataset.addValue(1, manufName, "2nd Quarter");
									}
								}

								else if (i == 2 && manufMonth >= 6 && manufMonth < 9) {
									try {
										dataset.incrementValue(1, manufName, "3rd Quarter");
									} catch (Exception e) {
										dataset.addValue(1, manufName, "3rd Quarter");
									}
								} else if (i == 3 && manufMonth >= 9 && manufMonth < 12) {
									try {
										dataset.incrementValue(1, manufName, "4th Quarter");
									} catch (Exception e) {
										dataset.addValue(1, manufName, "4th Quarter");
									}
								}
							}
						}

						chart = ChartFactory.createBarChart("Products Manufactured By Quarter", "Products",
								"Products Manufactured", dataset, PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
								"Product: {0}, Quarter: {1}, Quantity Manufactured: {2}", NumberFormat.getInstance()));

					} else
						proceed = false;
				}

				else {
					double[] manufsPerQ = new double[4];

					for (ManufactureSlip ms : App.productsManufactured) {
						if (ms.getDate().getYear() == new Date().getYear()) {
							if (unit.equals("All Products") || ms.getProduct().getName().equals(unit)) {
								int month = ms.getDate().getMonth();

								if (month < 3)
									manufsPerQ[0]++;
								else if (month < 6)
									manufsPerQ[1]++;
								else if (month < 9)
									manufsPerQ[2]++;
								else if (month < 12)
									manufsPerQ[3]++;

							}
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					if (manufsPerQ[0] + manufsPerQ[1] + manufsPerQ[2] + manufsPerQ[3] > 0)
						dataIn = 1;

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();
						String category = "Sales";

						if (!unit.equals("All Products"))
							category = category.concat(" (Product: " + unit + ")");
						dataset.addValue(manufsPerQ[0], "1st Quarter", category);
						dataset.addValue(manufsPerQ[1], "2nd Quarter", category);
						dataset.addValue(manufsPerQ[2], "3rd Quarter", category);
						dataset.addValue(manufsPerQ[3], "4th Quarter", category);

						chart = ChartFactory.createBarChart("Products Manufactured By Quarter", "Quarter",
								"Products Manufactured", dataset, PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
								"{0}, Products Manufactured: {2}", NumberFormat.getInstance()));

					}

					else
						proceed = false;
				}
			}

			// Products Produced By Month (Last Year)
			else if (lvl.equals("By Month")) {
				if (unit.equals("Compare Products")) {
					ArrayList<String> manufs = new ArrayList<String>();

					int curY = new Date().getYear();

					for (String product : manyUnits) {
						for (ManufactureSlip ms : App.productsManufactured) {
							if (ms.getProduct().getName().equals(product) && ms.getDate().getYear() == curY)
								manufs.add(product + "|" + ms.getDate().getMonth());
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					if (!manufs.isEmpty())
						dataIn = 1;

					if (dataIn != 0) {

						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = 0; i < 12; i++) {
							for (String ms : manufs) {
								String manufName = ms.split("\\|")[0];
								int manufMonth = Integer.parseInt(ms.split("\\|")[1]);

								if (manufMonth == i) {
									try {
										dataset.incrementValue(1, manufName, intToMonth(manufMonth));
									}

									catch (Exception e) {
										dataset.addValue(1, manufName, intToMonth(manufMonth));
									}
								}
							}
						}

						chart = ChartFactory.createBarChart("Products Manufactured By Month", "Products",
								"Products Manufactured", dataset, PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
								"Product: {0}, Month: {1}, Quantity Manufactured: {2}", NumberFormat.getInstance()));

					} else
						proceed = false;
				}

				else {
					int[] manufByM = new int[12];

					for (ManufactureSlip ms : App.productsManufactured) {
						if (ms.getDate().getYear() == new Date().getYear()) {
							if (unit.equals("All Products") || ms.getProduct().getName().equals(unit))
								manufByM[ms.getDate().getMonth()]++;
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					for (int check : manufByM) {
						if (check > 0) {
							dataIn = 1;
							break;
						}
					}

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = 0; i < 12; i++) {
							if (manufByM[i] > 0) {
								String category = "Products Manufactured";
								if (!unit.equals("All Products"))
									category = category.concat(" (Product: " + unit + ")");
								dataset.addValue(manufByM[i], intToMonth(i), category);
							}
						}

						chart = ChartFactory.createBarChart("Products Manufactured By Month", "Month",
								"Products Manufactured", dataset, PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
								"{0}, Products Manufactured: {2}", NumberFormat.getInstance()));

					} else
						proceed = false;
				}
			}

			// Products Produced By Week (Last Quarter)
			else if (lvl.equals("By Week")) {
				if (unit.equals("Compare Products")) {
					ArrayList<String> manufs = new ArrayList<String>();

					int curY = new Date().getYear();

					for (String product : manyUnits) {
						for (ManufactureSlip ms : App.productsManufactured) {
							if (ms.getProduct().getName().equals(product) && ms.getDate().getYear() == curY) {
								Calendar calendar = Calendar.getInstance();
								calendar.setTime(ms.getDate());
								int woy = calendar.get(Calendar.WEEK_OF_YEAR);
								manufs.add(ms.getProduct().getName() + "|" + woy);

							}
						}
					}

					int curM = new Date().getMonth();
					int curQ = 0;

					if (curM < 3)
						curQ = 1;
					else if (curM < 6)
						curQ = 2;
					else if (curM < 9)
						curQ = 3;
					else if (curM < 12)
						curQ = 4;

					int[] qWeeks = new int[2];

					Calendar cal = Calendar.getInstance(Locale.UK);
					cal.clear();
					cal.set(Calendar.YEAR, curY + 1900);

					if (curQ == 1) {
						cal.set(Calendar.MONTH, 2);
						cal.set(Calendar.DAY_OF_MONTH, 31);
						qWeeks[0] = 1;
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					else if (curQ == 2) {
						cal.set(Calendar.MONTH, 2);
						cal.set(Calendar.DAY_OF_MONTH, 31);
						qWeeks[0] = cal.get(Calendar.WEEK_OF_YEAR);
						cal.set(Calendar.MONTH, 5);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					else if (curQ == 3) {
						cal.set(Calendar.MONTH, 5);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[0] = cal.get(Calendar.WEEK_OF_YEAR);
						cal.set(Calendar.MONTH, 8);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					else if (curQ == 4) {
						cal.set(Calendar.MONTH, 8);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[0] = cal.get(Calendar.WEEK_OF_YEAR);
						cal.set(Calendar.MONTH, 11);
						cal.set(Calendar.DAY_OF_MONTH, 31);
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					// First Weeks Numbers
					cal.set(Calendar.MONTH, 3);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					int qTwoFirstWeek = cal.get(Calendar.WEEK_OF_YEAR);

					cal.set(Calendar.MONTH, 6);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					int qThreeFirstWeek = cal.get(Calendar.WEEK_OF_YEAR);

					cal.set(Calendar.MONTH, 9);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					int qFourFirstWeek = cal.get(Calendar.WEEK_OF_YEAR);

					// Check If Data Is There
					int dataIn = 0;

					for (String ms : manufs) {
						int mWeek = Integer.parseInt(ms.split("\\|")[1]);
						if (mWeek >= qWeeks[0] && mWeek <= qWeeks[1]) {
							dataIn = 1;
							break;
						}
					}

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = qWeeks[0]; i <= qWeeks[1]; i++) {
							for (String ms : manufs) {
								String manufName = ms.split("\\|")[0];
								int manufWeek = Integer.parseInt(ms.split("\\|")[1]);

								if (manufWeek == i) {
									int normalizeWeek = 0;

									switch (curQ) {
									case 2:
										normalizeWeek = qTwoFirstWeek;
										break;
									case 3:
										normalizeWeek = qThreeFirstWeek;
										break;
									case 4:
										normalizeWeek = qFourFirstWeek;
										break;
									}

									int dispWeek = manufWeek - normalizeWeek;
									if (dispWeek == 0)
										dispWeek++;

									if (dispWeek > 0) {

										try {
											dataset.incrementValue(1, manufName, "Week " + String.valueOf(dispWeek));
										}

										catch (Exception e) {
											dataset.addValue(1, manufName, "Week " + String.valueOf(dispWeek));
										}
									}

								}

							}
						}

						chart = ChartFactory.createBarChart("Products Manufactured By Week", "Products",
								"Products Manufactured", dataset, PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
								"Product: {0}, {1}, Quantity Manufactured: {2}", NumberFormat.getInstance()));

					}

					else
						proceed = false;
				}

				else {

					int curY = new Date().getYear();
					int curM = new Date().getMonth();
					int curQ = 0;

					if (curM < 3)
						curQ = 1;
					else if (curM < 6)
						curQ = 2;
					else if (curM < 9)
						curQ = 3;
					else if (curM < 12)
						curQ = 4;

					int[] qWeeks = new int[2];

					Calendar cal = Calendar.getInstance(Locale.UK);
					cal.clear();
					cal.set(Calendar.YEAR, curY + 1900);

					double[] dispVals = new double[cal.getActualMaximum(Calendar.WEEK_OF_YEAR) + 1];

					for (ManufactureSlip ms : App.productsManufactured) {
						if (ms.getDate().getYear() == new Date().getYear()) {
							if (unit.equals("All Products") || ms.getProduct().getName().equals(unit)) {
								Calendar calendar = Calendar.getInstance();
								calendar.setTime(ms.getDate());
								int woy = calendar.get(Calendar.WEEK_OF_YEAR);
								dispVals[woy]++;
							}
						}
					}

					if (curQ == 1) {
						cal.set(Calendar.MONTH, 2);
						cal.set(Calendar.DAY_OF_MONTH, 31);
						qWeeks[0] = 1;
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					else if (curQ == 2) {
						cal.set(Calendar.MONTH, 2);
						cal.set(Calendar.DAY_OF_MONTH, 31);
						qWeeks[0] = cal.get(Calendar.WEEK_OF_YEAR);
						cal.set(Calendar.MONTH, 5);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					else if (curQ == 3) {
						cal.set(Calendar.MONTH, 5);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[0] = cal.get(Calendar.WEEK_OF_YEAR);
						cal.set(Calendar.MONTH, 8);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					else if (curQ == 4) {
						cal.set(Calendar.MONTH, 8);
						cal.set(Calendar.DAY_OF_MONTH, 30);
						qWeeks[0] = cal.get(Calendar.WEEK_OF_YEAR);
						cal.set(Calendar.MONTH, 11);
						cal.set(Calendar.DAY_OF_MONTH, 31);
						qWeeks[1] = cal.get(Calendar.WEEK_OF_YEAR);
					}

					// First Weeks Numbers
					cal.set(Calendar.MONTH, 3);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					int qTwoFirstWeek = cal.get(Calendar.WEEK_OF_YEAR);

					cal.set(Calendar.MONTH, 6);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					int qThreeFirstWeek = cal.get(Calendar.WEEK_OF_YEAR);

					cal.set(Calendar.MONTH, 9);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					int qFourFirstWeek = cal.get(Calendar.WEEK_OF_YEAR);

					int dataIn = 0;

					for (int i = 0; i < dispVals.length; i++) {
						if (i >= qWeeks[0] && i <= qWeeks[1] && dispVals[i] > 0) {
							dataIn = 1;
							break;
						}
					}

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = 0; i < dispVals.length; i++) {
							double dispVal = dispVals[i];

							if (dispVal > 0) {
								int normalizeWeek = 0;

								switch (curQ) {
								case 2:
									normalizeWeek = qTwoFirstWeek;
									break;
								case 3:
									normalizeWeek = qThreeFirstWeek;
									break;
								case 4:
									normalizeWeek = qFourFirstWeek;
									break;
								}

								int dispWeek = i - normalizeWeek;
								if (dispWeek == 0)
									dispWeek++;

								String category = "Products Manufactured";
								if (!unit.equals("All Products"))
									category = category.concat(" (Product: " + unit + ")");

								if (dispWeek > 0) {

									try {
										dataset.incrementValue(dispVal, "Week " + String.valueOf(dispWeek), category);
									}

									catch (Exception e) {
										dataset.addValue(dispVal, "Week " + String.valueOf(dispWeek), category);
									}

								}
							}
						}

						chart = ChartFactory.createBarChart("Products Manufactured By Week", "Week",
								"Products Manufactured", dataset, PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
								"{0}, Products Manufactured: {2}", NumberFormat.getInstance()));

					}

					else
						proceed = false;
				}
			}

			// Products Produced By Day (Last Month)
			else if (lvl.equals("By Day")) {
				if (unit.equals("Compare Products")) {
					ArrayList<String> manufs = new ArrayList<String>();

					Calendar cal = Calendar.getInstance();
					cal.setTime(new Date());

					int curM = new Date().getMonth();
					int curY = new Date().getYear();
					int daysNum = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

					for (String product : manyUnits) {
						for (ManufactureSlip ms : App.productsManufactured) {
							if (ms.getDate().getYear() == curY && ms.getDate().getMonth() == curM) {
								if (ms.getProduct().getName().equals(product)) {
									Calendar calendar = Calendar.getInstance();
									calendar.setTime(ms.getDate());
									int day = calendar.get(Calendar.DAY_OF_MONTH);
									manufs.add(product + "|" + day);
								}
							}
						}
					}

					int dataIn = 0;

					if (!manufs.isEmpty())
						dataIn = 1;

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						String month = intToMonth(new Date().getMonth());

						for (int i = 1; i <= daysNum; i++) {
							for (String ms : manufs) {
								final String day = month + " " + i;
								String manufName = ms.split("\\|")[0];
								int manufDay = Integer.parseInt(ms.split("\\|")[1]);

								if (manufDay == i) {
									try {
										dataset.incrementValue(1, manufName, day);
									}

									catch (Exception e) {
										dataset.addValue(1, manufName, day);
									}
								}
							}
						}

						chart = ChartFactory.createBarChart("Products Manufactured By Day", "Products",
								"Products Manufactured", dataset, PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
								"Product: {0}, {1}, Quantity Manufactured: {2}", NumberFormat.getInstance()));

					}

					else
						proceed = false;
				}

				else {
					Calendar cal = Calendar.getInstance();
					cal.setTime(new Date());

					int curM = new Date().getMonth();
					int curY = new Date().getYear();
					int[] manufsByDay = new int[cal.getActualMaximum(Calendar.DAY_OF_MONTH)];

					for (ManufactureSlip ms : App.productsManufactured) {
						if (ms.getDate().getYear() == curY) {

							if (unit.equals("All Products") || ms.getProduct().getName().equals(unit)) {
								cal.setTime(ms.getDate());
								if (ms.getDate().getMonth() == curM) {
									manufsByDay[cal.get(Calendar.DAY_OF_MONTH) - 1]++;
								}
							}
						}
					}

					// Check If Data Is There
					int dataIn = 0;

					for (int check : manufsByDay)
						dataIn += check;

					if (dataIn != 0) {
						DefaultCategoryDataset dataset = new DefaultCategoryDataset();

						for (int i = 0; i < manufsByDay.length; i++) {
							final String day = intToMonth(curM) + " " + (i + 1);
							final int val = manufsByDay[i];
							String category = "Products Manufactured";
							if (!unit.equals("All Products"))
								category = category.concat(" (Product: " + unit + ")");
							if (val > 0)
								dataset.addValue(val, day, category);
						}

						chart = ChartFactory.createBarChart("Products Manufactured By Day", "Day",
								"Products Manufactured", dataset, PlotOrientation.VERTICAL, true, true, false);

						CategoryPlot cp = (CategoryPlot) chart.getPlot();
						BarRenderer renderer = (BarRenderer) cp.getRenderer();

						renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator(
								"{0}, Products Manufactured: {2}", NumberFormat.getInstance()));

					}

					else
						proceed = false;
				}
			}

			// Products Produced By Manufacturer (Last Year)
			else if (lvl.equals("By Manufacturer")) {
				if (unit.equals("Compare Products")) {
					ArrayList<String> mss = new ArrayList<String>();
					ArrayList<Worker> manufers = new ArrayList<Worker>();
					int curY = new Date().getYear();

					for (String product : manyUnits) {
						for (ManufactureSlip ms : App.productsManufactured) {
							if (ms.getDate().getYear() == curY) {
								if (ms.getProduct().getName().equals(product)) {
									Worker worker = ms.getManufacturer();

									boolean manuferExists = false;

									for (Worker w : manufers) {
										if (w.getEmail().equals(worker.getEmail())) {
											manuferExists = true;
											break;
										}
									}

									if (manuferExists == false)
										manufers.add(worker);

									mss.add(product + "|" + worker.getName() + "|" + worker.getEmail());
								}

							}
						}

					}

					int[] msByW = new int[manufers.size()];

					for (int i = 0; i < manufers.size(); i++) {
						Worker manufer = manufers.get(i);

						for (String ms : mss) {
							String mWE = ms.split("\\|")[2];

							if (mWE.equals(manufer.getEmail())) {
								msByW[i]++;
							}
						}

					}

					// Check If Data Is There
					int dataIn = 0;

					if (!manufers.isEmpty())
						dataIn = 1;

					if (dataIn != 0) {
						DefaultPieDataset dataset = new DefaultPieDataset();

						for (int i = 0; i < manufers.size(); i++) {
							String mName = manufers.get(i).getName();
							final int manufNum = msByW[i];

							dataset.setValue(mName, manufNum);
						}

						chart = ChartFactory.createPieChart("Products Manufactured By Worker", dataset, true, true,
								false);

						PiePlot pp = (PiePlot) chart.getPlot();

						pp.setToolTipGenerator(
								new StandardPieToolTipGenerator("Worker: {0}, Products Manufactured: {1} ({2})",
										new DecimalFormat(), new DecimalFormat("0%")));
						PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator("{2}", new DecimalFormat(),
								new DecimalFormat("0%"));
						pp.setLabelGenerator(gen);

						pp.setSimpleLabels(true);
					}

					else
						proceed = false;
				}

				else {
					ArrayList<String> manufers = new ArrayList<String>();
					ArrayList<ManufactureSlip> allMs = new ArrayList<ManufactureSlip>();

					for (ManufactureSlip ms : App.productsManufactured) {
						if ((unit.equals("All Products") || ms.getProduct().getName().equals(unit))
								&& ms.getDate().getYear() == new Date().getYear()) {
							String manufer = ms.getManufacturer().getName() + "|" + ms.getManufacturer().getEmail();

							boolean manuferExists = false;

							for (String m : manufers) {
								if (m.equals(manufer)) {
									manuferExists = true;
									break;
								}
							}

							if (manuferExists == false)
								manufers.add(manufer);

							allMs.add(ms);

						}
					}

					int[] msByW = new int[manufers.size()];

					for (ManufactureSlip ms : allMs) {
						for (int i = 0; i < manufers.size(); i++) {
							String manufer = manufers.get(i);
							String manufW = ms.getManufacturer().getName() + "|" + ms.getManufacturer().getEmail();
							if (manufer.equals(manufW)) {
								msByW[i]++;
							}
						}
					}

					// Check If Data Is There.
					int dataIn = 0;

					for (int check : msByW)
						dataIn += check;

					if (dataIn != 0) {
						DefaultPieDataset dataset = new DefaultPieDataset();

						for (int i = 0; i < manufers.size(); i++) {
							final String manufer = manufers.get(i).split("\\|")[0];
							dataset.setValue(manufer, msByW[i]);
						}

						String categoryP = " ";
						if (!unit.equals("All Products"))
							categoryP = categoryP.concat(" (Product: " + unit + ")");

						String chartName = "Products Manufactured";

						chart = ChartFactory.createPieChart(chartName + categoryP, dataset, true, true, false);

						PiePlot pp = (PiePlot) chart.getPlot();

						pp.setToolTipGenerator(
								new StandardPieToolTipGenerator("Worker: {0}, Products Manufactured: {1} ({2})",
										new DecimalFormat(), new DecimalFormat("0%")));
						PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator("{2}", new DecimalFormat(),
								new DecimalFormat("0%"));
						pp.setLabelGenerator(gen);

						pp.setSimpleLabels(true);
					}

					else
						proceed = false;
				}
			}
		}

		c.gridy = 2;
		c.weighty = 0.6;

		if (proceed == true) {
			p.add(new ChartPanel(chart), c);

			if (unit.equals("Compare Products")) {
				unitsComparedDisp.setVisible(true);
				String uDisp = "Products Compared: ";

				for (int i = 0; i < manyUnits.length; i++) {
					String in = manyUnits[i];

					if (i == 0)
						uDisp = uDisp.concat(in);
					else
						uDisp = uDisp.concat(", " + in);
				}

				unitsComparedDisp.setText(uDisp);
			}

			else {
				unitsComparedDisp.setVisible(false);
			}

			j.pack();
		}

		else {
			int x = j.getX();
			int y = j.getY();
			unitsComparedDisp.setVisible(false);

			p.add(new JLabel("No Data"), c);
			j.pack();
			j.setSize((int) (j.getWidth() * 1.3), j.getHeight());
			j.setLocation(x, y);
		}

	}

	private static String intToMonth(int val) {
		String monthString;

		switch (val) {
		case 0:
			monthString = "January";
			break;
		case 1:
			monthString = "February";
			break;
		case 2:
			monthString = "March";
			break;
		case 3:
			monthString = "April";
			break;
		case 4:
			monthString = "May";
			break;
		case 5:
			monthString = "June";
			break;
		case 6:
			monthString = "July";
			break;
		case 7:
			monthString = "August";
			break;
		case 8:
			monthString = "September";
			break;
		case 9:
			monthString = "October";
			break;
		case 10:
			monthString = "November";
			break;
		case 11:
			monthString = "December";
			break;
		default:
			monthString = "Invalid month";
			break;
		}
		return monthString;
	}
}
