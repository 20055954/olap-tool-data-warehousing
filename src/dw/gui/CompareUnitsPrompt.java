package dw.gui;

//* By Andrey Shevyakov, 2015 * //

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import dw.app.App;
import dw.model.ManufactureSlip;
import dw.model.Product;

// Stock Items Comparison
public class CompareUnitsPrompt extends JDialog {

	private static final long serialVersionUID = 7137786759184200679L;
	
	public CompareUnitsPrompt(String option, String selectedUnit, String saleLoc, String optionsLvl, String perfMeasurer, JFrame j, JPanel analyticalP,
			GridBagConstraints cOther, String units, JTextField comparedUnitsDisp, boolean manufPs)
	{
		try { setIconImage(ImageIO.read(new File("res/icon.png")));}
		catch (IOException e) {}
		
		App.appGUI.setEnabled(false);
		
	    JLabel header = new JLabel("Select " + units + " To Compare", SwingConstants.CENTER);
	    header.setBorder(BorderFactory.createBevelBorder(1));
	    
    	ArrayList <String> manufUnits = null;

	    if (manufPs)
	    {
	    	manufUnits = new ArrayList <String> ();
	    	
	    	if (manufUnits.isEmpty()) manufUnits.add(App.productsManufactured.get(0).getProductName());
	   
	    	for (ManufactureSlip ms: App.productsManufactured)
	    	{
	    			String in = ms.getProduct().getName();
	    			
	    			boolean unique = true;
	    		
	    			for (String check: manufUnits)
	    			{
	    				if (check.equals(in))
	    				{
	    					unique = false;
	    					break;
	    				}
	    			}
	    			
	    			if (unique == true) manufUnits.add(in);
	    	}	
	    	
	    }
	   
	    ArrayList <JCheckBox> unitSelection = new ArrayList <JCheckBox> ();

	    if (manufPs == false)
	    {
	    	for (Product p : App.products)
	    	unitSelection.add(new JCheckBox(p.getName()));
	    }
	    
	    else
	    {
	    	for (String in : manufUnits)
	    	unitSelection.add(new JCheckBox (in));
	    }
	    
	    
	    JPanel checks = new JPanel(new GridBagLayout());
	    GridBagConstraints c = new GridBagConstraints();
	    
	    c.gridx = 0;
	    c.gridy = 0; 
	    c.weighty = 10 / unitSelection.size();
	    c.weightx = 1;
	    c.fill = GridBagConstraints.HORIZONTAL;
	    
	    for (int i = 0; i < unitSelection.size(); i++)
	    {
	    	checks.add(unitSelection.get(i), c);
	    	c.gridy += 1;
	    }
	    
	    checks.setPreferredSize(new Dimension (200, (int) checks.getPreferredSize().getHeight()));
	    JScrollPane checksS = new JScrollPane(checks);
	    checksS.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
	    checksS.setPreferredSize(new Dimension (200,200));
	    
	    JButton button = new JButton ("Compare Selected " + units);
	    
	    JPanel content = new JPanel (new GridBagLayout());
	    
	    c.gridx = 0;
	    c.gridy = 0; 
	    c.weighty = 0.1;
	    c.fill = GridBagConstraints.HORIZONTAL;
	    content.add(header,c);
	    
	    c.gridy = 1;
	    c.weighty = 0.8;
	    content.add(checksS,c);
	    
	    c.gridy = 2;
	    c.weighty = 0.1;
	    content.add(button,c);
	    
		setTitle("Compare " + units);
		setContentPane(content);
		pack();
		setVisible(true);
		setResizable(false);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
		
		// Resuming The Workflow On Closure
		addWindowListener(new WindowListener(){

			@Override
			public void windowActivated(WindowEvent arg0) {}

			@Override
			public void windowClosed(WindowEvent arg0) {
				App.appGUI.toFront();
				App.appGUI.setEnabled(true);
				dispose();}

			@SuppressWarnings("static-access")
			@Override
			public void windowClosing(WindowEvent arg0) {
				App.appGUI.units.setSelectedIndex(0);
				App.appGUI.toFront();
				App.appGUI.setEnabled(true);
				dispose();}

			@Override
			public void windowDeactivated(WindowEvent arg0) {}

			@Override
			public void windowDeiconified(WindowEvent arg0) {}

			@Override
			public void windowIconified(WindowEvent arg0) {}

			@Override
			public void windowOpened(WindowEvent arg0) {}
			
		});
		
		// Submit Listener
		button.addActionListener(new ActionListener() {

			@SuppressWarnings("static-access")
			@Override
			public void actionPerformed(ActionEvent arg0) {

				ArrayList <String> unitsSelected = new ArrayList<String>();
				
				for (JCheckBox c : unitSelection)
				{
					if (c.isSelected()) unitsSelected.add(c.getText());
				}
				
				if (!unitsSelected.isEmpty()) 
				{
					App.appGUI.unitsSelected = unitsSelected.toArray(new String[unitsSelected.size()]);
					App.appGUI.createChartPanel(option, selectedUnit, saleLoc, optionsLvl, perfMeasurer, j, analyticalP, cOther, dim, App.appGUI.unitsSelected, comparedUnitsDisp);
					App.appGUI.toFront();
					App.appGUI.setEnabled(true);
					dispose();
				}
			}
		
			
			
		});
	}

}
