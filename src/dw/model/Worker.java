package dw.model;

//* By Andrey Shevyakov, 2015 * //

import dw.app.App;

public class Worker {

	private String name;
	private String email;
	private String dept;
	private String location;
	
	public Worker(String name, String email, String dept, String location)
	{
		this.name = name;
		this.email = email;
		if (email == null) this.email = "Unknown E-Mail";
		this.dept = dept;
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public boolean isUnique () {
		for (Worker compare : App.staff) 
		if (compare.getEmail().equalsIgnoreCase(getEmail()) && compare.getName().equalsIgnoreCase(getName())) return false;
		return true;
	}

}
