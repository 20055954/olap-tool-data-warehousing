package dw.model;

//* By Andrey Shevyakov, 2015 * //

import java.util.Date;

public class ManufactureSlip {
	
	private Worker manufacturer; 
	private Product product; 
	private Date date; 
	
	public ManufactureSlip(Worker manufacturer, Product product, Date date)
	{
		this.manufacturer = manufacturer;
		this.product = product;
		this.date = date;
	}

	@Override
	public String toString() {
		return "ManufactureSlip [manufacturer=" + manufacturer + ", product=" + product + ", date=" + date + "]";
	}
	
	public String getProductName () {
		return product.getName();
	}

	public Worker getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(Worker manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
