package dw.model;

//* By Andrey Shevyakov, 2015 * //

import java.util.ArrayList;
import java.util.Date;

public class Promotion {

	private String desc;
	private Date date;
	private ArrayList <String> locs;
	
	public Promotion(String desc, Date date, String loc)
	{
		this.locs = new ArrayList<String>();
		this.desc = desc;		
		this.date = date;
		
		String [] inLocs = loc.split(",");
		
		for (String in : inLocs)
		this.locs.add(in.trim());
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public ArrayList<String> getLocs() {
		return locs;
	}

	public void setLocs(ArrayList <String> locs) {
		this.locs = locs;
	}
	
}
