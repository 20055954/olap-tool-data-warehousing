package dw.model;

//* By Andrey Shevyakov, 2015 * //

import java.util.Date;

public class Sale {
	
	private Product product;
	private int qtSold;
	private String location; 
	private Worker seller; 
	private Date date;
	private double revenue;
	
	public Sale (Product product, int qtSold, String location, Worker seller, Date date, double revenue)
	{
		this.product = product;
		this.qtSold = qtSold;
		this.location = location;
		this.seller = seller;
		this.date = date;
		this.revenue = revenue;
	}

	
	@SuppressWarnings("deprecation")
	public boolean same (Sale sale)
	{
		if (product.getName().equals(sale.getProduct().getName()) 
				&& product.getPrice() == sale.getProduct().getPrice()  
				&& qtSold == sale.getQtSold()
				&& seller.equals(sale.getSeller())
				&& date.getDay() == sale.getDate().getDay() && date.getMonth() == sale.getDate().getMonth()
				&& date.getYear() == sale.getDate().getYear()) return true;
		else return false;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQtSold() {
		return qtSold;
	}

	public void setQtSold(int qtSold) {
		this.qtSold = qtSold;
	}

	public double getRevenue() {
		
		if (product.getPrice() != -1) revenue = product.getPrice() * qtSold; 

		return revenue;
	}
	
	public void setRevenue (double revenue) {
		this.revenue = revenue;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Worker getSeller() {
		return seller;
	}

	public void setSeller(Worker seller) {
		this.seller = seller;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
