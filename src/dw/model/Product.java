package dw.model;

//* By Andrey Shevyakov, 2015 * //

public class Product {
	
	String name;
	double price; 
	int qtLeft; 
	
	public Product (String name, double price, int qtLeft) {
		this.name = name; 
		try
		{this.price = price;}
		catch (Exception e) {this.price = -1;}
		try
		{this.qtLeft = qtLeft;}
		catch (Exception e) {this.qtLeft = -1;}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", price=" + price + "]";
	}

}
