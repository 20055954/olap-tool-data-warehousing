package dw.db;

//* By Andrey Shevyakov, 2015 * //

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import com.lambdaworks.redis.RedisURI;

import dw.model.Promotion;

public class RedisConnector extends RedisClient {

	private RedisConnection<String, String> connection;

	// Connecting To The "Marketing" DB
	public RedisConnector(String hostName, int port, String password) {

		RedisURI params = RedisURI.create("redis://" + password + "@" + hostName + ":" + port);
		connection = connect(params);
		connection.select(0);

	}

	// Retrieving The Store Locations
	public List<String> retrieveSalesLocations() {
		
		List<String> salesLocations = new ArrayList<String>();
		long salesLocationsNum = connection.llen("Sales Locations");

		for (int i = 0; i < salesLocationsNum; i++) {
			salesLocations.add(connection.lindex("Sales Locations", i));
		}

		return salesLocations;
		
	}
	
	// Retrieving The Promotions
	public List<Promotion> retrievePromotions() {
		
		List<Promotion> promotions = new ArrayList<Promotion>();
		long promotionsNum = connection.llen("Promotions");

		for (int i = 0; i < promotionsNum; i++) {
			String result = connection.lindex("Promotions", (long) i);
			String[] resultSplit = result.split("\\|\\|\\|");
			DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

			try {
				promotions.add(
						new Promotion(resultSplit[1], dateFormat.parse(resultSplit[0]), resultSplit[2]));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		return promotions;
		
	}

	public void closeConnection() {
		
		connection.close();
		shutdown();
		
	}
}
