package dw.listeners;

//* By Andrey Shevyakov, 2015 * //

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// Button Listener To Allow For Excel Spreadsheets Loading
public class ExcelActionListener implements ActionListener{

	protected String location;
	
	public ExcelActionListener (String location)
	{
		this.location = location;
	}

	@Override
	public void actionPerformed(ActionEvent e) {}

}
